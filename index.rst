.. CLARIN documentation master file, created by
   sphinx-quickstart on Thu Apr  9 13:48:29 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==============================================================
CLARIN:EL – User Manual 
==============================================================

Welcome to the user manual of CLARIN:EL!

.. note:: Cite this version: Pouli, K., et al. (2023). *The CLARIN:EL User Manual* (v. 1.0).

* Creator: **Kanella POULI**

* Contributors: **Juli BAKAGIANNI, Dimitris GALANIS, Penny LABROPOULOU, Maria GAVRIILIDOU** 

* Proofreading: **Iro TSIOULI**

* Set up and implementation: **Dimitris GALANIS**


.. toctree::
   :maxdepth: 2 
   :caption: Introduction

   ./all/0_Introduction/AboutClarin.rst 
   ./all/0_Introduction/AboutManual.rst
   ./all/0_Introduction/Contact.rst

.. toctree::
   :maxdepth: 3 
   :caption: Basic concepts

   ./all/1_BasicConcepts/RegisterAsUser.rst
   ./all/1_BasicConcepts/Users.rst
   ./all/1_BasicConcepts/TypesOfResources.rst 
   ./all/1_BasicConcepts/Accessible.rst
   ./all/1_BasicConcepts/LandingPage.rst
   ./all/1_BasicConcepts/MainMenu.rst
      
.. toctree::
   :maxdepth: 2 
   :caption: Navigating the infrastructure
   
   ./all/2_Navigating/Browse.rst
   ./all/2_Navigating/Search.rst
   ./all/2_Navigating/ResourceView.rst
   ./all/2_Navigating/Process.rst
   
.. toctree::
   :maxdepth: 2 
   :caption: Creating resources

   ./all/3_Creating/publicationLifecycle.rst
   ./all/3_Creating/HowTo.rst
   ./all/3_Creating/Editor.rst
   ./all/3_Creating/Upload.rst

.. toctree::
   :maxdepth: 2 
   :caption: Data
   
   ./all/4_Data/DataPreparation.rst
   ./all/4_Data/FileFormats_new.rst

.. toctree::
   :maxdepth: 2 
   :caption: Metadata
   
   ./all/5_Metadata/Necessity.rst
   ./all/5_Metadata/Fair.rst
   ./all/5_Metadata/Mandatory.rst
   ./all/5_Metadata/GuidelinesGen.rst
   ./all/5_Metadata/GuidelinesSpec.rst
   ./all/5_Metadata/Examples.rst
   ./all/5_Metadata/AllXMLdescriptions.rst
   ./all/5_Metadata/Full.rst
         
.. toctree::
   :maxdepth: 2 
   :caption: Managing resources
   
   ./all/6_Managing/Managing.rst

.. toctree::
   :maxdepth: 2 
   :caption: Appendix
   
   ./all/Appendix/Licences.rst   
   ./all/Appendix//Publications.rst

.. glossary::

   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
