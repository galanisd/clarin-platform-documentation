.. _HowTo:

Important information about all LRTs
########################################

Before you embark on creating a language resource for `CLARIN:EL <https://inventory.clarin.gr/>`_, please keep in mind the following:

* the resource must comply with our `terms of service <https://www.clarin.gr/en/content/terms-service>`_ [#]_;
* the legal status of the resource must be clear and a licence must be provided;
* the resource can be created either by using the :ref:`interactive editor<Editor>` or by :ref:`uploading an XML file<Upload>` with at least the mandatory metadata per resource type;
* for the resource to be complete, the physical data (henceforth **content files**) must be provided as well: :ref:`directly to the infrastructure<UploadData>` or indirectly with the use of an external link.

Depending on the type of resource you would like to create, please refer to the respective chapters for the mandatory elements needed to create a metadata record for:

1. a :ref:`corpus<corpusM>`,

2. a :ref:`tool<toolM>`,

3. a :ref:`lexical/conceptual resource<LCRM>`,

4. a :ref:`language description<LDM>`
  

.. [#] See also our `privacy policy <https://www.clarin.gr/en/content/privacy-policy-summary>`_.