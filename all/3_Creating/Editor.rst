.. _Editor:

##################################################
How to create a resource by using the editor
##################################################

.. attention:: This section is dedicated to the **basic steps** you must take to create a resource using the metadata editor [#]_. Although the example showcases how a corpus is created, the steps (except indicated otherwise) are common for all LRTs. 

In order to describe a resource through the editor you must be signed in. If so, visit your :ref:`dashboard<Dashboard>` and click on :guilabel:`create resources`. In the new window, choose the resource type you wish to create by clicking on **Go to form**. 

.. image:: CreateResources.png
    :width: 1200px

Before you start, make sure you have checked which are the :ref:`mandatory<Mandatory>` elements in order to be able to create a metadata record for

* a :ref:`corpus<corpusM>`,

* a :ref:`tool<toolM>`,

* a :ref:`lexical/conceptual resource<LCRM>`,

* a :ref:`language description<LDM>`

Step 1: Name your resource
***************************

The first step is to give your resource a name; in order to avoid confusion, the system checks if this name is already in use. If this is the case, you are presented with a list of resources which match your proposed resource name (wholly or partly). If you want to create a new record, you have to use another name [#]_. 

.. image:: NameFound.png
    :width: 1200px

If the name is not found, you can proceed to the next phase by clicking the **create** button [#]_. 

.. image:: ResourceName.png
    :width: 1200px

.. attention:: After a tool name has been checked, you will be presented with a different screen asking you whether you would like it to be **integrated in the CLARIN:EL infrastructure as a service**.

.. image:: CreateTool.png
    :width: 1200px

Depending on your answer, the respective box will/will not be checked in the editor. You can change your decision anytime.

.. image:: DemoTool.png
    :width: 1200px 

.. _UploadData:

Step 2: Upload the resource data
*************************************

.. attention:: Althought the functionality is available for all types of resources, it is advisable to **integrate tools as services**, in collaboration with the CLARIN:EL technical team, rather than upload the software.   

The content files must be in a **compressed folder** in one of the following formats: **.zip, .tgz, .gz, .tar** [#]_. When naming the folder you must use the latin alphabet and leave no spaces between the words. See :ref:`here <DataPreparation>` how to prepare the data before uploading.

_`Skip and upload later`
=========================

When you click on **create**, a new window appears asking you to upload your data. If you want to, you can **skip** this step and upload your data later.

.. image:: Skip.png
    :width: 1200px

If you decide to upload your content files at a later time, simply visit the editor and select the :guilabel:`Data` section where the **upload** button is found. 

.. image:: EditorUploadData.png
    :width: 1200px

_`Upload immediately` 
=======================

Otherwise, you can directly upload your data to the metadata record you are about to create. A series of screens (as shown in the image below) will be presented to guide you through:

1. select the dataset, 

2. upload it,  

3. see the details of your upload when it is completed, and

4. click on the **finish** button to go back to the editor. 

.. image:: UploadDataAll.png
    :width: 600px

_`Assign to distribution` 
==============================

As shown in the last image, the dataset is **not assigned** to any distribution; that means that you have to create an **association** between the dataset and the form or delivery channel through which it is distributed (e.g. a CD-ROM, a link from where the dataset can be downloaded, etc.). In addition, you must define the **licence terms** under which this form of distribution is available. 

.. attention:: You do not have to associate the dataset with the distribution form and licence terms upon uploading the dataset; you can create the link at anytime. To do so, click on the :guilabel:`Distribution` section.

.. image:: AssociateDataset.png
    :width: 1200px

First select the form of distribution from the dropdown list and then click on the name of the zip file. Save the changes you have made. The next time you check the metadata record you will see that the dataset is assigned.

.. image:: DatasetAssigned.png
    :width: 1200px

You will **not be able to submit a record for publication**, unless all uploaded datasets are assigned to their respective distributions. Upon submitting a record, you will be notified and transferred to the editor.

.. image:: InvalidRecord.png
    :width: 1200px

Replace content files
=========================

If you need to replace the content files you have uploaded, create a new compressed file **with the correct data** and the **same file name (and format)**.  From the :guilabel:`Data` section choose to upload data. You will be presented with a warning message.

.. image:: Replace2.png
    :width: 1200px

Click on **replace dataset** and if the replacement is successful, you will be notified by a message at the bottom right side of your page. The new dataset is indicated by the **new upload date** as shown in the image below.

.. image:: ReplaceSuccess.png
    :width: 1200px

Save the metadata record. You will be transferred to the resource view page while being informed that the resource has been updated successfully.

.. image:: UpdatedSuccess.png
    :width: 1200px

Delete content files
=========================

Once you have assigned the content files to a distribution, the delete action in the :guilabel:`Data` section will be deactivated and you will be presented with a warning message.

.. image:: DeleteDataset.png
    :width: 1200px

To delete them, follow the next steps:

1. Go to the :guilabel:`Distribution` section.
2. Select the void option in the respective metadata field (this action will **disconnect** the data from the distribution). 

.. image:: AssociateDataset.png
    :width: 1200px

3. Save the metadata record.
4. Choose **again** to edit the metadata record.
5. Go to the :guilabel:`Data` section. The delete button is now activated.
6. Delete the data.

You will see a message that the action has been successfully completed.

.. image:: FileDeleted.png
    :width: 1200px
 
7. Save the metadata record.

Step 3: Fill in the mandatory metadata
******************************************

Whichever type of resource you choose, you have to provide information on some :ref:`mandatory<Mandatory>` metadata elements (different per resource type) which are distributed in several **sections** (organized horizontally) and various **tabs** (presented vertically) in the editor. 

.. _Save:

During the creation process you can stop any time and save your record **as draft** [#]_. You will not be able to **save** your record unless you have filled in all the mandatory metadata. Every time you click on **save**, the metadata you have entered are checked; each time mandatory metadata are missing or have false values, you will get a message prompting you to correct your errors. 

.. image:: RequiredMessage.png
    :width: 1200px

After closing this message a new highlighted area appears above the sections. It contains the **path** (section > tab) to each one of the missing/incorrect metadata. When you click on it, you are **automatically transferred** to the respective section tab where the missing metadata are highlighted with a vertical red line.

.. image:: RequiredNew.png
    :width: 1200px

Upon checking whether you can save this record after filling in the required metadata, you might be informed again with a message that more metadata are needed. This happens because some of the values you have entered have generated new requirements (these are the :ref:`mandatory upon condition<allM>` metadata). Again, the new metadata you must fill in appear at the top of the editor page.

.. image:: RequiredMessage2.png
    :width: 1200px

This message might appear several times before all the necessary metadata have been filled in. After the process has been completed, you will be finally allowed to save the metadata record.

.. image:: RequiredMessageFinal.png
    :width: 1200px

Step 4: View the created record
**********************************

After you click on **proceed**, you get a message that the metadata record has been successfully created and you are transferred to the resource view page.

.. image:: RecordSaved.png
    :width: 1200px

You can still :ref:`edit<Edit>` the metadata you have added before you decide to submit the record for publication.

.. [#] Henceforth **editor**.
.. [#] Otherwise, you can edit one of the resources in the list, provided you have the rights to do so.
.. [#] The button is deactivated when matches of the name are found. As soon as you use a new name, it becomes activated.
.. [#] If the files are available in multiple formats, (e.g. in XML, TXT and PDF formats), you are advised to package them in different compressed files by data format.
.. [#] You can even save as draft a record with only the resource name.