.. _Upload:

##################################################
How to create a resource by uploading an XML file
##################################################

You can create any type of resource by uploading an XML file with at least the :ref:`mandatory<Mandatory>` metadata.

.. attention:: This section is dedicated to the **basic steps** you must take to create a resource by XML upload. Although the example showcases how a corpus is created, the steps (except indicated otherwise) are common for all LRTs. See :ref:`here<Examples>` specific XML files describing different types of resources.

In order to upload an XML you must be :ref:`signed in<SignAsUser>`. If so, visit your :ref:`dashboard<Dashboard>` and click on :guilabel:`upload resources`. In the new window, you are presented with three tabs, as shown in the image below. 

.. image:: UploadRecords.png
    :width: 1200px 

First you must validate your XML description file, i.e. you must upload it to be checked. Although you can skip that step, it's highly recommended and it will save you time; the validation shows inconsistencies or missing metadata, as in the image below. 

.. image:: UploadFailed.png
    :width: 1200px

Once you have corrected them, try to validate the file again. If everything is as it should be you will be notified that the xml file is valid.

.. image:: UploadValid.png
    :width: 1200px

Then, you can proceed to the next tab to successfully upload your file. If your resource falls into one of the following categories [#]_ you must click the appropriate box before uploading the XML file.  

.. image:: UploadCheckBox.png
    :width: 1200px

You will be informed that the process has been successfully completed and you will be transferred to the resource view page.

.. image:: UploadXMLsuccess.png
    :width: 1200px

.. [#] See :ref:`here<resourceCategories>` the various categories of resources.