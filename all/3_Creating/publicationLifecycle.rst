.. _publicationLifecycle:

#############################
The lifecycle of a resource
#############################

`CLARIN:EL <https://inventory.clarin.gr/>`_ is an infrastructure for language resources and tools/services which reside in the repositories of the members of the :ref:`CLARIN:EL network<AboutClarin>`; they are harvested and presented in the central inventory.
A resource (i.e., a metadata record and, optionally, content files uploaded with it) goes through a set of states in the process of being prepared for publication on the inventory, as depicted in the following figure:

.. figure:: PublicationLifecycle.png
    :width: 1200px
	
The states are:

- **new resource**: A :ref:`curator<Curator>` creates a resource, by creating a metadata record through the :ref:`metadata editor<Editor>` [#]_ or by uploading an :ref:`XML metadata file<Upload>` and, optionally, content files.

- **draft (internal)**: When using the interactive editor, the curator can save [#]_ the metadata record as draft, even without filling all :ref:`mandatory<Mandatory>` elements; only compliance as to the data type of the elements is checked (e.g. elements that take URL must be filled in with the accepted pattern).

- **syntactically valid (ingested)**: The system checks that the metadata record complies with the :ref:`CLARIN:EL metadata schema<FullSchema>` and all :ref:`mandatory<Mandatory>` elements are filled in. The curator can continue to edit it until satisfied with the description and can then submit it for publication. 

- **submitted (assigned for validation)**: Once the curator submits the resource for publication, the record is no longer editable. The :ref:`supervisor<Supervisor>` receives an email to assign [#]_ :ref:`validators<Validator>`. Depending on the resource type (and the uploaded content files, if any), the resource is checked by the metadata and legal validator [#]_. The validation aims to check the consistency of the description; it doesn't include any qualitative evaluation. When validators identify a problem, they contact the curator for further information and may ask the curator to edit the metadata; in such cases, the status of the resource is changed to **syntactically valid** again and the curator is notified to make the appropriate amendments.

- **published**: When the validator(s) have **approved** a resource, the supervisor is notified by email and must publish it. After being made visible in the CLARIN:EL inventory the metadata record cannot be edited any more. It can return to the syntactically valid status only if **(requested to be) unpublished**.

.. attention:: Only the metadata records created via the editor can be saved as **draft**. The XML metadata files cannot be imported unless they are **syntactically valid** (which is the status they are automatically set to).

.. [#] Henceforth **editor**.
.. [#] See the differences in **save as draft** and **save** :ref:`here<Save>`.
.. [#] When there are more than one supervisors in a repository, one should first be assigned to the resource. See :ref:`here<AssignS>` how to do this. Then, the supervisor must choose and :ref:`assign<Assign>` validators.
.. [#] When no content files have been uploaded, the resource is considered automatically legally valid.
