.. _managing:

##################################
Actions on resources
##################################

I. Per resource status and user type
****************************************

.. _ActFrom:

You can manage your resources by performing actions on them. All actions are available from the list of resources you have **permissions** on. To reach this list go to your :ref:`dashboard<Dashboard>` and choose:

* :guilabel:`View my resources` from :ref:`My resources<MyResources>` if you are a :ref:`curator<Curator>` (**C**), 

* :guilabel:`View my supervision tasks` from :ref:`My repository<MyRepo>` if you are a :ref:`supervisor<Supervisor>` (**S**),

* :guilabel:`View my validation tasks` from my :ref:`Validation tasks<ValidationTasks>` if you are a :ref:`validator<Validator>` (**V**).

You will be presented with a list of resources. Each row is dedicated to a single resource and has an **Actions** button as shown in the image. When you click on it, you will see a dropdown list. 

.. image:: ActionsList2.png
    :width: 1200px

Alternatively, you can choose a resource by clicking on its name and see the available actions from its :ref:`view page<ViewAction>` [#]_.

.. image:: ViewPageActions.png
    :width: 1200px

The actions you see are the actions **you have the right to perform** depending on your :ref:`role<RegisteredUsers>` and the resource status. The table below shows the actions each type of user has permission to do at every stage of the :ref:`publication lifecycle<publicationLifecycle>`. Click on the action and you will be transferred to the respective section to learn more about it.

+---------------------------------------+-----+----------+---------+--------+---------+---------------+-----------+
| **Action/Status**                     |draft|synt.valid|submitted|approved|published|requested [#]_ |unpublished|
+=======================================+=====+==========+=========+========+=========+===============+===========+
| :ref:`edit metadata<Edit>`            |**C**|**C, S**  |**S**    |**S**   |n/a      |n/a            |**S**      |
+---------------------------------------+-----+----------+---------+--------+---------+---------------+-----------+
| :ref:`copy record<Copy>`              |n/a  |**C, S**  |**C, S** |**C, S**|**C, S** |**C, S**       |**C, S**   |
+---------------------------------------+-----+----------+---------+--------+---------+---------------+-----------+
| :ref:`submit for publication<Submit>` |n/a  |**C, S**  |n/a      |n/a     |n/a      |n/a            |n/a        |
+---------------------------------------+-----+----------+---------+--------+---------+---------------+-----------+
| :ref:`assign supervisor<AssignS>`     |n/a  |n/a       |**S**    |n/a     |n/a      |n/a            |n/a        |
+---------------------------------------+-----+----------+---------+--------+---------+---------------+-----------+
| :ref:`assign validator<Assign>`       |n/a  |n/a       |**S**    |n/a     |n/a      |n/a            |n/a        |
+---------------------------------------+-----+----------+---------+--------+---------+---------------+-----------+
| :ref:`validate<Validate>`             |n/a  |n/a       |**V**    |n/a     |n/a      |n/a            |n/a        |
+---------------------------------------+-----+----------+---------+--------+---------+---------------+-----------+
| :ref:`publish<Publish>`               |n/a  |n/a       |n/a      |**S**   |n/a      |n/a            |**S**      |
+---------------------------------------+-----+----------+---------+--------+---------+---------------+-----------+
| :ref:`unpublish<Unpublish>`           |n/a  |n/a       |n/a      |n/a     |**S**    |**S**          |n/a        |
+---------------------------------------+-----+----------+---------+--------+---------+---------------+-----------+
| :ref:`request to unpublish<Request>`  |n/a  |n/a       |n/a      |n/a     |**C**    |n/a            |n/a        |
+---------------------------------------+-----+----------+---------+--------+---------+---------------+-----------+
| :ref:`create new version<CreateNew>`  |n/a  |n/a       |n/a      |n/a     |**C, S** |**C, S**       |n/a        |
+---------------------------------------+-----+----------+---------+--------+---------+---------------+-----------+
| :ref:`delete metadata<Delete>`        |**C**|**C, S**  |**S**    |**S**   |n/a      |n/a            |**S**      |
+---------------------------------------+-----+----------+---------+--------+---------+---------------+-----------+

* n/a = not applicable

Actions can be performed on a **single** resource (from the resource :ref:`view page<ViewAction>`) or on **multiple** resources (via the list of resources you have **permissions** on). To select more than one resources click on the box on the left of their name and choose an action from the **action box** at the top of the page. If you select resources **the status of which is different**, you will see **only the available actions** for all resources (e.g. export metadata as shown in the image below).

.. image:: CommonActions.png
    :width: 1200px

.. _Filters:

In your list of resources, there are also filters to facilitate your tasks. The following table shows the available filters per user type. Click on the filter and you will be transferred to the respective section to learn more about it.

+--------------------------------------------+------------+----------------+---------------+
|**Filter**                                  |**Curator** | **Supervisor** | **Validator** |
+============================================+============+================+===============+
|:ref:`resource type<typesOfResources>`      | x          | x              | x             |
+--------------------------------------------+------------+----------------+---------------+
|:ref:`status<publicationLifecycle>`         | x          | x              | x             |
+--------------------------------------------+------------+----------------+---------------+
|:ref:`has data<UploadData>`                 | x          | x              | x             |
+--------------------------------------------+------------+----------------+---------------+
|:ref:`processable<Processable>`             | x          | x              | x             |
+--------------------------------------------+------------+----------------+---------------+
|:ref:`action required<allActions>`          |            | x              |               |
+--------------------------------------------+------------+----------------+---------------+
|:ref:`metadata valid<Validate>`             |            | x              | x             |
+--------------------------------------------+------------+----------------+---------------+
|:ref:`legally valid<Validate>`              |            | x              | x             |
+--------------------------------------------+------------+----------------+---------------+
|CLARIN:EL compatible service                |            | x              | x             |
+--------------------------------------------+------------+----------------+---------------+
|:ref:`requested for unpublish<Request>`     |            | x              |               |
+--------------------------------------------+------------+----------------+---------------+
|:ref:`validator assignment required<Assign>`|            | x              |               |
+--------------------------------------------+------------+----------------+---------------+
|:ref:`for information<resourceCategories>`  |            | x              |               |
+--------------------------------------------+------------+----------------+---------------+
|:ref:`metaresources<resourceCategories>`    |            | x              |               |
+--------------------------------------------+------------+----------------+---------------+

.. _allActions:

II. The actions
******************

.. _Edit:

Edit
=========

By clicking on **edit metadata** you will be transferred to the resource metadata record in the :ref:`metadata editor<Editor>` [#]_. You can edit it as many times as you like. After editing you can save the resource **as draft** or if you have filled in all the :ref:`mandatory<Mandatory>` metadata and you are satisfied with the resource description you can finally **save** it. By saving it, it accquires the **syntactically valid** status. It remains editable and can also be edited by the repository supervisor (something not possible while it was at the draft stage where it is accessible only by the curator). 

* If you are a **curator** you will be able to edit your resource from the draft to the syntactically valid stage.

* If you are a **supervisor** you will be able to edit a resource from the syntactically valid stage up to when it is approved and then again when it is :ref:`unpublished<Unpublish>`.

A metadata record will have to go through editing **again** if the metadata validator has rejected it. This will result in the resource returning to the **syntactically valid** status again. As a curator you will receive an email with the validator's comments and you will have to edit the resource and submit it for publication when you are over.

.. _Copy:

Copy record
==================

You can copy a metadata record after you have saved it and it is **syntactically valid**.

.. image:: Copy.png
    :width: 1200px

Click on the action and a new window will open, in which you must name the resource and define its version [#]_. 

.. image:: Copy2.png
    :width: 1200px

When you are done, click on **Create copy**. You will be informed that the copy of the metadata record was successfully created while transferred to its view page.

.. image:: Copy3.png
    :width: 1200px
  
The copied metadata record is **syntactically valid** and must go through the stages described in the :ref:`publication lifecycle<publicationLifecycle>`. 
  
.. _Submit:

Submit for publication 
=========================

This action takes place on **syntactically valid** resources. When you are satisfied with the metadata description of a resource (either as a curator or supervisor), you can **submit it for publication**. 

.. image:: SubmittedSuccess.png
    :width: 1200px

* If you are a **supervisor** you will receive an email asking you to *"assign validators to the following record, as it is ready for legal and metadata validation"*.

.. _AssignS:

Assign supervisor
======================

.. attention:: This action is available only to **supervisors** and is required only when there are **more than one supervisors** in a repository. If there is only one supervisor in a repository, the system automatically assigns all resources to him/her.

Once a resource has been submitted for publication you will receive an email asking you to assign yourself as supervisor to the resource: *Please assign yourself as supervisor to the following record and then assign legal and metadata validators, as it is ready for validation.* 

.. image:: AssignSupervisor.png
    :width: 1200px

By clicking on the action, a new window will open asking you to select the user you wish to make supervisor of the resource.

.. image:: SelectSupervisor.png
    :width: 1200px

After you have selected the supervisor, click on submit and you will see a success message at the bottom right side of your page.

.. image:: Success.png
    :width: 1200px

.. _Assign:

Assign validator
===================

.. attention:: This action is available only to **supervisors**. In order to be able to assign a resource to the legal and metadata validators, you must first have assigned these roles to users in your repository. See :ref:`here<UserManagement>` how to do this.

When a resource is submitted for publication, you will receive an email informing you that you must assign it to validators. If the submitted resource does **not** have content files, it is automatically considered **legally valid** [#]_ and you only have to assign a metadata validator. 

.. image:: AssignMetadataValidator.png
    :width: 1200px
 
If the submitted resource **has** content files, you have to assign a legal and a metadata validator.

.. image:: AssignLegalValidator.png
    :width: 1200px

The procedure must be repeated for each validator type separately. You must click on the action (assign legal or metadata validator) and then you will be presented with a new window.

.. image:: Validator.png
    :width: 1200px

Depending on the number of validators existing in your repository you will see a dropdown list. Choose the validator you want and then click on **submit**. A success message will appear at the right down side of your window. 

When you return to the resources in your :ref:`supervision tasks<MyRepo>`, you will be able to see the validators you have assigned to each resource, or the need to assign validators, as well as any comments they might have made during validation. 

.. image:: ValidatorAssigned.png
    :width: 1200px

.. _Publish:

Publish 
===========

.. attention:: This action is available only to **supervisors**. 

Once a resource is both legally and metadata valid, its status changes from **submitted** to **approved**. You will be notified by email that *"it has been approved by the validators, therefore it is ready for publication"*.

.. image:: Publish.png
    :width: 1200px

A message at the bottom right side of the page will inform you upon the successful publication of the resource and the resource curator will also be notified by email.

.. image:: Message.png
    :width: 1200px

.. _Request:

Request to unpublish 
=========================

If you are a **curator** and you think that a published resource shouldn't be in the central inventory, you can ask for it to be unpublished.

.. image:: Request.png
    :width: 1200px

When you click on the action, a new window opens asking you to declare the reasons for your request.

.. image:: Reason.png
    :width: 1200px

Once you indicate the reasons, press **Request to unpublish**; you will see a message, at the bottom right side of the page, that your request has been successfully submitted and in the list of resources, the resource status has changed.

.. image:: TobeUnpublished.png
    :width: 1200px

.. _Unpublish:

Unpublish
===============

.. attention:: This action is available only to **supervisors**. 

Only **published** or **requested for unpublish** records can be unpublished. 

.. image:: Unpublish.png
    :width: 1200px

When the action is successfully completed the resource status is set to **unpublished**.

.. image:: successUnpublish.png
    :width: 1200px

.. _CreateNew:

Create New Version
========================

Once a resource is published, you can create a new version of it, if needed, e.g. due to updates.

.. image:: CreateNewVersion.png
    :width: 1200px

Click on the action and a new window will open. Fill in the new version number and the date and then click on **create new version**.

.. image:: Version.png
    :width: 1200px

You will see a success message at the bottom right side of your page.

.. image:: Success.png
    :width: 1200px

.. _Delete:

Delete metadata
=====================

If you are not satisfied with the description of a resource, you can delete its metadata record.

.. image:: DeleteMetadata.png
    :width: 1200px

When the action has been completed successfully, you will see a success message at the bottom right side of your page. The metadata record you deleted does no longer exist.

.. image:: SuccessDelete.png
    :width: 1200px


.. _Validate:

Validate 
==============

.. attention:: This action is available only to **validators**. 

When a resource has been assigned to you, you will receive an email informing you that you must validate it. From your :ref:`validation tasks<ValidationTasks>` find the resource and click on its name. You **cannot** perform any actions from the resource list but you **can** from the resource view page. Click on the actions dropdown list as shown in the image below. You will be presented with the appropriate option [#]_ of validation. The process described is the same both for metadata and legal validation.

.. image:: MetadataValidate.png
    :width: 1200px

When you click on the action, a new window will open asking you to accept or reject the metadata record.

.. image:: Choose.png
    :width: 1200px

* If you choose **accept** and submit, the window closes and the resource is **metadata valid**. 

* If you choose **reject**, a new window opens where you must write the reasons for rejecting the resource. 

.. image:: Reject.png
    :width: 1200px

When you return to the resources found in your validation tasks, you will see your comments.

.. image:: Comments.png
    :width: 1200px

Your comments will be communicated to the resource creator by email. The resource status will then return to **syntactically valid** so that it can be edited again according to your remarks. 

.. [#] You will not be able to see the resource view page until you have provided all the correct :ref:`mandatory<Mandatory>` elements. Only then the metadata record, which is syntactically valid, can be **saved** and presented as a view page.
.. [#] Resources which have been requested to unpublish.  
.. [#] Henceforth **editor**.
.. [#] Version 1.0.0 is automatically given to resources in which the :ref:`mandatory <Mandatory>` element has not been filled in.
.. [#] Since there are no content files, there is no need for licence policy; hence the resource is considered legally valid.
.. [#] Resources that do not have content files are automatically considered **legally valid** and only go through metadata validation.