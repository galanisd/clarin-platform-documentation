.. _Publications:

Publications
###############

A full list of publications and presentations is available `here <https://www.clarin.gr/en/documentation/publications>`_.