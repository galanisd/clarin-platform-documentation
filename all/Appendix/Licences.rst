.. _Licences:

############################
Information on Legal Issues
############################

Click on the links to read about the `Privacy Policy <https://www.clarin.gr/en/content/privacy-policy-summary>`_ and the `Terms of Service <https://www.clarin.gr/en/content/terms-service>`_ (these are found at the bottom of each page in the infrastructure, as shown in the image below). 

.. image:: ContactNew.png
    :width: 1200px

.. tip:: Please, also check the `Recommended licensing scheme for Language Resources <https://www.clarin.gr/en/support/legal>`_ which has been created to help you limit the complexity of licensing.