.. _Accessible:

################################################
When is the content of a resource accessible?
################################################

.. attention:: This section is for resource descriptions which **come with** content files. To see the different types of resources found in the CLARIN:EL central inventory, please see :ref:`here<typesOfResources>`.

While navigating through the infrastructure you will see metadata records many of which, as explained earlier, come with content files. In order for the content files of a resource to be accessible two criteria must be met beforehand: 

1. a resource needs to be provided under an **open access licence** (check `here <https://www.clarin.gr/en/support/legal>`_ the *Recommended licensing scheme for Language Resources*), and 

2. the resource content files must **have been uploaded** or **stored** at an access point. 

This holds true both for those resources available through the infrastructure and via external links. For the latter, several other conditions must be met upon:

* the link must be provided in the appropriate metadata field in the metadata editor [#]_/xml file, 

* the link must work (not be broken), and 

* the content of the link must be well maintained.

Independently of the point of access to the data, directly or indirectly, the following table shows the possible combinations of actions which allow or not for downloading.  

+----------------------+--------------------------------------+--------------+
| Does the licence     | Have the content files been uploaded |Can the data  |
| provide Open Access? |(datasets, tools, lexica, etc.)?      |be downloaded?|
+======================+======================================+==============+
| **Yes**              | **Yes**                              | **Yes**      |
+----------------------+--------------------------------------+--------------+
| Yes                  | No                                   | No           |
+----------------------+--------------------------------------+--------------+
| No                   | Yes                                  | No           |
+----------------------+--------------------------------------+--------------+
| No                   | No                                   | No           |
+----------------------+--------------------------------------+--------------+

The content of the table is visualised in the following flowchart:  

.. image:: accessible.png
    :width: 1500px
    :align: center	

.. [#] Henceforth **editor**.