.. _RegisterAsUser:

###########################
Registration/Sign in
###########################

This chapter provides information to the users

1. who already have an academic account and can use it to :ref:`sign in<SignAsUser>`,
2. who do **not** have an academic account and **must first** :ref:`register<Register>` so as to create a personal account which they will later on use to :ref:`sign in<SignAsUser>`.

.. _Register:

Register
*************

.. attention:: Skip this section, if you already have an accademic account which you can use to :ref:`sign in<SignAsUser>`. 

To register in `CLARIN:EL <https://inventory.clarin.gr/>`_, follow the next steps:

* Click on the **sign in** button at the top right of the page.

.. image:: register.png
    :width: 1200px

* In the next window choose to **Register with personal account**.

.. image:: register2.png
    :width: 1200px

* Then provide all the necessary information in the form that appears and click on **Register with personal account**. 

.. image:: register3.png
    :width: 1200px

* You will receive an email with a link to confirm your address and agree to the CLARIN:EL Terms of Service & Privacy Policy.

.. image:: register4.png
    :width: 1200px

* Once confirmed, your account is activated. After you have registered, every time you would like to use the infrastructure, simply **sign in**.

.. _SignAsUser:
 
Sign in
***********

You can sign in using either your personal or academic (Greek or Federation) account which will uniquely associate you with your organization or academic institution and consequently with the respective repository. If you are not affiliated with a specific organization/ academic institution, you will be directed to the **Hosted Resources Repository**. 

.. image:: register2.png
    :width: 1200px

To use your academic account click on **Greek academic login**. You will be redirected to a new page where you should type in the box the name of your organization/academic institution and then select **confirm**.

.. image:: GRNET.png
    :width: 1200px

Again, in the new window that opens, use your academic credentials and finally click on **login**. 

.. image:: Login.png
    :width: 1200px

You are signed in!  
