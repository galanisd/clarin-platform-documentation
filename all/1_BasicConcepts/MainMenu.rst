.. _MainMenu:

############################
Menu for registered users
############################

After you have signed in, the menu offers five options:

:guilabel:`CLARIN:EL portal`: a link to the infrastructure `portal <https://www.clarin.gr/el>`_, where you can find information about the network, news and announcements;

:guilabel:`Dashboard`: a link to your personalized dashboard, serving as an access point to the metadata editor [#]_, the resources you have created, your tasks and processing jobs.

:guilabel:`Help`: a link to the infrastructure documentation and guidelines (this document), where you can learn how to navigate, create and manage resources, etc.;

:guilabel:`Your name`: a link to your profile, which you can edit.

:guilabel:`Exit`: an icon to log out.

.. image:: Menu.png
    :width: 1200px

.. [#] Henceforth **editor**.

.. _Dashboard:

Dashboard
*******************

The Dashboard serves both as an overview page for your activities (where you can find information on your resources, tasks, processing jobs) and an entry point to :ref:`create resources<Editor>` and use :ref:`workflows<Workflow>` to process resources. As shown below, it contains nine different sections. Sections 2 and 7-9 are slightly different depending on your role.

.. image:: DashboardALLNew.png
    :width: 1200px

.. tip:: See here how the dashboard looks for a :ref:`curator<CuratorDashboard>`, a :ref:`validator<ValidatorDashboard>` and a :ref:`supervisor<SupervisorDashboard>`.

1. Welcome
================

This section is introductory and informs you on what you can do here.

.. image:: Dashboard1.png
    :width: 700px

.. _Profile:

2. Profile
======================

The second section is dedicated to your profile. For supervisors there is an extra functionality, namely :guilabel:`Manage Repository Users`, as shown below.

.. image:: ProfileAll.png
    :width: 1200px

If you click on :guilabel:`View my profile`, you will be directed to a new page.

.. image:: Profile.png
    :width: 1200px

Some of your personal information is editable [#]_. Click on the pencil symbol next to Personal information and you will be directed to a new page where you can edit your personal data. After you have filled in the fields, save your changes.

.. image:: EditProfile.png
    :width: 1200px

.. [#] You cannot change the repository you are affiliated with, since this is done automatically during your registration or the roles you have, which have been assigned to you by the supervisor.

.. _UserManagement:

User management
----------------------

.. attention:: This functionality is available **only to supervisors**.

If you click on :guilabel:`Manage Repository Users`, you will be directed to a page where all the users of your repository are listed. You can search for a specific user by using the search box on top. Once you have found the user, you must select the box on the left of the user's name. 

.. image:: UserManagement2.png
    :width: 1200px

Then you are provided with two actions to choose from: you can make the user a **legal** or a **metadata** validator. The same action can be also performed from the action box on the top of the user list. Whatever you choose, a new window will open asking you to confirm your decision.

.. image:: MakeValidator.png
    :width: 1200px

By clicking on **make legal validator** you are giving the selected user permission to validate resources.

.. _CreateResources:

3. Create resources
========================

.. image:: CreateResources2.png
    :width: 1200px
	
In this section you see the total number of the resources you have created, **but not the resources themselves** [#]_. By clicking on :guilabel:`+ Create resources`, you are transferred to a new page where you have to select the type of resource you wish to create.

.. image:: CreateResources.png
    :width: 1200px

Before you proceed to the :ref:`editor<Editor>` by clicking **Go to form**, please, see the guidelines for the creation of

* a :ref:`corpus<corpusM>`,

* a :ref:`tool<toolM>`,

* a :ref:`lexical/conceptual resource<LCRM>`,

* a :ref:`language description<LDM>`

using the schema :ref:`mandatory<Mandatory>` elements.

.. [#] The section :ref:`My resources<MyResources>` has a list with what you have created. 

.. _UploadResources:

4. Upload resources
========================

.. image:: UploadResources.png
    :width: 1200px

You can click on :guilabel:`+ Upload resources` if you already have one (or more) description/s in XML format. A new window will open where you are provided with several options. To find out more, see :ref:`here<Upload>`.

.. _SelectWorkflow:

5. Select workflow
========================

.. image:: NewWorkflow.png
    :width: 1200px

By clicking on :guilabel:`+ Select workflow` you are transferred to the workflow registry. See :ref:`here<Workflow>` what the available services can do.

.. _ProcessingTasks:

6. Processing Tasks
========================

In this section you can see all your processing tasks and their results.

.. image:: TokenizationFinished.png
    :width: 1200px

You can directly download the files that have been successfully processed from here. If you wish to have an overview, click on :guilabel:`View all processing tasks`.

.. image:: ViewMyProcessingTasks.png
    :width: 1200px

Your submitted processing tasks are presented in a table organized in six columns:

* the *first* one contains the name of the input resource, provided you have selected it from the :ref:`processable<Processable>` corpora available through `CLARIN:EL <https://inventory.clarin.gr/>`_; if you have uploaded your own resource, the column is empty,

* the *second* has the name of the zipped file; all the infrastructure processable corpora come in an archive.zip,

* the *third* contains the name of the output based on the name of the input; again if there was no input name, the column is empty,

* the *fourth* is the submission date,

* the *fifth* is the status of the processing, and

* the *sixth* is the download button.


.. _MyResources:

7. My resources
========================

This section, contains all the resources you have created via the :ref:`editor<Editor>` or :ref:`XML upload<Upload>`.

.. image:: MyResources.png
    :width: 1200px

You can either select a resource by clicking on its name or, if you wish to have an overview of all the resources, you can click on :guilabel:`View my resources`. 

.. image:: ViewMyResources.png
    :width: 1200px

In this page, on the left, there are :ref:`filters<Filters>` to help you sort out the resources depending on their type, status, data or processability. You can apply as many filters as you like and then clear them by clicking on the button above them. 

.. image:: ResourcesFiltersZoom.png
    :width: 1200px

.. note:: As a supervisor you will also see a **search box for curators**. Use the email of a curator to find only the resources created by them.

.. image:: CuratorSearch.png
    :width: 1200px

As you can see, each resource occupies a row separated in four columns:

* the *first*, provides some basic information on the resource,

* the *second*, presents the name of the curator, supervisor and validator (if the resource has been validated),

* the *third*, has a button for the available actions, and

* the *fourth* is the resource status.

To learn more about the actions you can perform on a resource see :ref:`here<Managing>`.

.. _ValidationTasks:

8. Validation tasks
========================

.. attention:: This section is visible only to **validators**.

Here you can see the list of all the resources that have been assigned to you for validation. You can select a resource by clicking on its name; you will be transferred to its view page. In case you have already validated this resource, a pop up message will inform you that you no longer have rights on it. 

.. image:: Inaccessible.png
    :width: 1200px

If you wish to have an overview of all the resources under validation (completed or not), you can click on :guilabel:`View my validation tasks`.

.. image:: ViewMyValidations.png
    :width: 1200px

In this page, on the left, there are :ref:`filters<Filters>` to help you sort out the resources. You can apply as many filters as you like and then clear them by clicking on the button above them. 

.. image:: ValidationsFiltersZoomCom.png
    :width: 1200px

As you can see, each resource occupies a row separated in three columns:

* the *first*, provides some basic information on the resource,

* the *second*, presents the names of the curator, supervisor and validators, and

* the *third* is the resource status. The information whether the resource is legally/metadata valid also appears here.

In addition, there is a box with the comments the validator has made, if any. See :ref:`here<Validate>` how you can validate a resource.

.. _MyRepo:

9. My repository
========================

.. attention:: This section is visible only to **supervisors**.

This section, contains all the resources of your repository, independently of who has created them. Remember, that, if you want to see the resources you have created, you must click on :ref:`my resources<MyResources>`.

.. image:: MyRepo.png
    :width: 1200px

You can either select a resource by clicking on its name or, if you wish to have an overview of all the resources in your repository, you can click on :guilabel:`View my supervision tasks`. In this page, on the left, there are :ref:`filters<Filters>` to help you sort out the resources depending on their type, status, data etc. You can apply as many filters as you like and then clear them by clicking on the button above them. The image below shows only the resources for which a validator must be assigned (based on the relevant filter). 

.. image:: SupervisorFilterZoom.png
    :width: 1200px

As you can see, each resource occupies a row separated in four columns:

* the *first*, provides some basic information,

* the *second*, presents the names of the curator, supervisor and validator (if the resource has been validated),

* the *third*, has a button for the available actions, and

* the *fourth* is the resource status, along with information on whether it has been validated or not.

To learn more about the actions you can perform on a resource see :ref:`here<Managing>`.


Help
*****************

This link directs you to the infrastructure :ref:`manual<AboutManual>` where information is documented about resources, users, rights and processes. You can search for a specific issue of interest using the search box or go through the various chapters and sections. 

.. image:: Documentation.png
    :width: 1200px


Your name
*****************

By clicking on your name you are transferred to your :ref:`profile<Profile>` page. 

Exit
*****************

If you no longer want to be signed in, you can log out by clicking on this icon.