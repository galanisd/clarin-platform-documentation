.. _Users:

########
Users  
########

Users have various rights depending on whether they have registered or not. Unregistered users can navigate the infrastructure with some permissions. Advanced permissions associated with the :ref:`creation<Editor>`, :ref:`management<Managing>` and :ref:`processing<Process>` of resources are given only to registered users according to the policy of the infrastructure. 

Registered vs Non registered
******************************

The central inventory presents the published LRTs from all repositories and can be accessed with or without registration. As concerns the **consumption** of resources, there is only one extra permission the registered user has which is to use the tools and services, as shown in the table below. 

.. tip:: **Click** on each of the inventory uses to find out more!

+---------------------------------------------------------+--------------------+----------------+
|                                                         | **Non registered** | **Registered** |
+---------------------------------------------------------+--------------------+----------------+
|:ref:`Browse<Browse>` the inventory                      | Yes                | Yes            |
+---------------------------------------------------------+--------------------+----------------+
|:ref:`Search<Search>` using keywords or filters         | Yes                | Yes            |
+---------------------------------------------------------+--------------------+----------------+
|:ref:`View<ResourceView>` all the metadata records       | Yes                | Yes            |
+---------------------------------------------------------+--------------------+----------------+
|:ref:`Download<Access>` resources [#]_                   | Yes (some)         | Yes (all)      |
+---------------------------------------------------------+--------------------+----------------+
|:ref:`Use<Process>` all tools/services                   | **Νο**             | Yes            |
+---------------------------------------------------------+--------------------+----------------+

.. _RegisteredUsers:

Types of registered users
***************************

Once you have registered, you must :ref:`sign in<SignAsUser>` if you wish to be directed to your repository. This will enable you to engage in the :ref:`the lifecycle of a resource<publicationLifecycle>` by assuming one of the following roles.

.. _Curator:

1. **Curator**: the curator is responsible for creating resources and uploading their content files, as well as managing (editing, updating, etc.) them and finally submitting them for publication. 

.. note:: By :ref:`signing in<SignAsUser>`, you **automatically** obtain the **curator** status in your repository.

.. _Validator:

2. **Validator**: the validator is assigned resources by the supervisor in order to check whether the metadata described (and the content files uploaded) are consistent - if not, the resource returns to the curator to be edited again according to the validator's comments.

.. note:: See :ref:`here<Assign>` how a user becomes validator.  

.. _Supervisor:

3. **Supervisor**: the supervisor has the final word before a resource is made public and is also the only one able to unpublish it, if necessary.

.. note:: See :ref:`here<AssignS>` how a user becomes supervisor.

Each one controls a number of resources on which :ref:`actions<managing>` can be performed via the :ref:`dashboard<Dashboard>` and the :ref:`resource view page<ViewAction>`.

.. [#] Unregistered users can **only** download resources provided under open licences (e.g. Creative Commons) while registered users can download **all** :ref:`downloadable<Accessible>` resources.
