.. _Dashboards.rst:

############################
User Dashboard
############################

.. _CuratorDashboard:

:guilabel:`Dashboard for curators`

.. image:: DashboardCurator.png
    :width: 1200px

------------------------------------------------

:guilabel:`Dashboard for validators`

.. _ValidatorDashboard:

.. image:: ValidatorNew.png
    :width: 1200px

------------------------------------------------

:guilabel:`Dashboard for supervisors`

.. _SupervisorDashboard:

.. image:: SupervisorNew.png
    :width: 1200px

