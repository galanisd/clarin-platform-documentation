.. _LandingPage:

#########################
Central Inventory Page
#########################

The inventory home page of the `CLARIN:EL <https://inventory.clarin.gr/>`_ infrastructure provides several options to the user who hasn't signed in or registered yet. 

.. image:: LandingPage1.png
    :width: 1200px

At the top right side of the page, there are three buttons:

:guilabel:`CLARIN:EL portal`: a link to the infrastructure `portal <https://www.clarin.gr/el>`_, where you can find information about the network, news and announcements, presentations and bibliography;

:guilabel:`Help`: a link to the infrastructure documentation and guidelines (this document), where you can learn how to navigate, create and manage resources, etc.;

:guilabel:`Sign in`: a link to a page where you can :ref:`register<Register>` or :ref:`sign in<SignAsUser>`.

Following, there is a search box with an option on each side: **browse** on the left and **search** on the right. These are entry points to the central inventory. You can :ref:`browse<Browse>` it as a whole or use :ref:`filters and keywords<Search>` to retrieve a subset of the LRTs that match your criteria.

.. _PrefilteredSelections:

In the middle of the page you can see two columns where specific groups of LRTs are presented. 

.. image:: LandingPage2.png
    :width: 1200px

The first column contains subsets of tools and/or services grouped according to whether:


* they are provided as `services <https://inventory.clarin.gr/workflows/>`_ in the infrastructure,

* they can be `downloaded <https://inventory.clarin.gr/search/?downloadable_tools__term=Downloadable%20Tools>`_, and

* they can be `accessed online <https://inventory.clarin.gr/search/?additional_online_services__term=Additional%20Online%20Services>`_ through external links.

In the same way, the datasets are groupped in the next column as those which can be:

* `processed <https://inventory.clarin.gr/search/?processable_datasets__term=Processable%20Datasets>`_,

* `downloaded <https://inventory.clarin.gr/search/?downloadable_datasets__term=Downloadable%20Datasets>`_, and

* used only to `provide information <https://inventory.clarin.gr/search/?meta_resources_initial_page__term=Metaresources>`_.

At the bottom of the page there is a `link <https://www.clarin.gr/el/about/what-is-clarin>`_ for users who wish to learn more about the CLARIN:EL community and possibly join in.

.. image:: Join.png 
    :width: 1200px