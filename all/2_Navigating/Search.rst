.. _Search:

#############
Searching
#############

The search box is found in the inventory home page of CLARIN:EL. You can use simple keywords or, as shown in the image, special characters for `Lucene advanced queries <http://www.lucenetutorial.com/lucene-query-syntax.html>`_ [#]_. 

.. image:: Search.png
    :width: 1200px

You have also the option to use **filters**. To do so, click on the browse/search button to be transferred to the central inventory. On the left side of the page, you can see all the available filters.

.. image:: Filters.png
    :width: 1200px
 
The filters' value lists are closed, but once you click on the arrow next to the filters they open. 

.. image:: FiltersOpen.png
    :width: 1200px
 
If you wish to remove the filter(s) applied, either click on the **clear all filters** button or on the **x** next to the filter name. 

.. image:: RemoveFilters.png
    :width: 1200px

In the central inventory you can use the following filters:

* :guilabel:`Repository`: It groups the LRTs of each repository separately.

* :guilabel:`Resource type`: It groups the LRTs by their type, i.e. corpora, tools/services, lexical/conceptual resources, language descriptions. 

* :guilabel:`Media type`: It groups the LRTs depending on their medium, i.e. text, video, audio, image. 

* :guilabel:`Languages`: It groups the LRTs depending on their language(s) or the language(s) they can process.

* :guilabel:`Language Variety`: It groups the LRTs depending on their language variety or the language variety they can process.

* :guilabel:`Linguality Type`: It groups the LRTs depending on whether they are monolingual, bilingual or multilingual.

* :guilabel:`Multilinguality Type`: It groups the LRTs depending on whether they are parallel or comparable (this filter applies only to bilingual and multilingual resources).

* :guilabel:`Domains`: It groups the LRTs according to the various fields of knowledge which have been used to classify it or its contents.  

* :guilabel:`Time Coverage`: It groups the LRTs depending on the period of time their contents cover (for instance, `resources of the 16th century <https://inventory.clarin.gr/search?time_coverage__term=16%CE%BF%CF%82%20%CE%B1%CE%B9.>`_). 

* :guilabel:`Geographic Coverage`: It groups the LRTs depending on the geographic region(s) of their contents (for instance, `resources related somehow to France <https://inventory.clarin.gr/search?geographic_coverage__term=France>`_). 

* :guilabel:`Processable`: It groups the corpora which can be processed within `CLARIN:EL <https://inventory.clarin.gr/>`_ according to whether they have the technical features which make them compatible with the CLARIN:EL integrated services of the workflow registry. 

* :guilabel:`Annotation Type`: It groups the LRTs based on their annotation(s) (for example, `bilingual or multilingual resources which are aligned <https://inventory.clarin.gr/search/?annotation_type__term=Alignment>`_).

* :guilabel:`Processing Service`: It groups only the tools which have been integrated in `CLARIN:EL <https://inventory.clarin.gr/>`_ as services.

* :guilabel:`Service Function`: It groups services according to the function they perform (e.g. `services for lexicon creation <https://inventory.clarin.gr/search/?function__term=Lexicon%20creation>`_).

* :guilabel:`Licences`: It groups LRTs according to their licence(s) (for instance resources under `CC-BY 4.0 licence <https://inventory.clarin.gr/search/?licence__term=CC-BY-4.0>`_).

.. attention:: For a resource to be filtered, and presented as a result to the user, the respective metadata must have been filled in by the curator. 

.. [#] For the time being the advanced queries can only be used for search in the central inventory, not in the search boxes found in :ref:`my resources<MyResources>`, :ref:`my validation tasks<ValidationTasks>`, :ref:`my repository<MyRepo>`.