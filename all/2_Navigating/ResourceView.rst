.. _ResourceView:

######################
Viewing & Downloading
######################

For each resource a metadata record is available providing descriptive and technical information as well as supporting documentation and other useful material. 

.. attention:: Although the resource view page is accessible to all users, some of its functionalities (e.g. the :guilabel:`Process` button) are available only to users who are :ref:`signed in<SignAsUser>`.  

The chosen example showcases a `corpus metadata record <http://hdl.handle.net/11500/ATHENA-0000-0000-23FF-A>`_. The layout of the view page is the **same for all types of resources**. What is different is the **content of the sections** which depends on the resource type. Each view page is split in two sections with several subparts.

.. _UpperSection:

The upper section 
*********************

It contains the resource **name** and **short name** (if any), **type**, **version**, and :ref:`PID <Findability>` followed by a **description**. Underneath the description there is a tag indicating that this resource is :guilabel:`processable`. On the side there is a **language selection button**. By default, all metadata records are presented in English (**en**), but if you choose **el** you will see all the metadata which have been added in Greek as well. Following, there is the repository logo and just underneath a citation text. You can **copy** the text just by clicking on the icon as shown in the image.

.. image:: ResourceView6.png
    :width: 1200px

.. _ViewAction:

The resource view page, provides extra functionalities when you :ref:`sign in<SignAsUser>` (provided you have assumed a :ref:`role<RegisteredUsers>` in your repository). The difference for the signed in user is the :guilabel:`actions` box shown in the image below.

.. image:: Actions.png
    :width: 1200px

.. tip:: More about the actions you can perform on a resource :ref:`here<Managing>`.

Under the description, there are boxes providing information on some of the resource **key features**. For corpora and lexical/conceptual resources these are: the **language(s)** covered, **keywords** (describing the contents), the resource **domain** and **subclass**. Depending on other metadata filled in, the boxes could also present the resource **coverage** as concerns **time** and **space**, as shown in the following image.

.. image:: ResourceView5.png
    :width: 1200px

In the metadata records for **tools/services** these boxes show their **function** and whether they are **language dependent**.

.. image:: ResourceView15.png
    :width: 1200px

.. _LowerSection:

The lower section 
**********************

The lower section is designed to have the following tabs: :ref:`Overview <Overview>`, :ref:`Technical <Technical>`, :ref:`Relations <Relations>`, :ref:`Access <Access>`. The combination of tabs appearing in each view page derives from the resource type and the metadata which have (or have not) been filled in. The selected tab each time changes from blue to orange.

.. image:: Tabs.png
    :width: 1200px

.. _Overview:

1. Overview
============

This tab contains some of the :ref:`mandatory metadata<corpusM>`, i.e. the **language(s)** and **linguality** of the corpus part (here text) as well as information on the annotations (if any). Here you will also see the **genre** and **category** of the corpus. The next column gives you the option to **export** the metadata in a file [#]_ and below this you can find out who is the resource **provider** and **contact** details, such as an email and/or a landing page which you can access to learn more about the resource. 
	
.. image:: ResourceView7.png
    :width: 1200px

In case there are more than one corpus parts, these are placed vertically on the left most column and you can see their features by clicking on the **medium** (the chosen part is highlighted in orange, as shown below).

.. image:: TextVideo.png
    :width: 1200px

The Overview tab for tools/services contains information on the **input and output features**, i.e. **language(s)**, **data format(s)**, etc.

.. image:: ResourceView16.png
    :width: 1200px

The bottom of the page in all cases provides related **papers** and **documentation**. In the case of corpora and lexical/conceptual resources there is also information on whether **personal** and **sensitive** data are included in the resource.

.. image:: ResourceView8.png
    :width: 1200px

.. _Technical:

2. Technical
==============

.. attention:: The Technical tab appears **only** in the view pages of **tools/services**!

It provides information on the **intended** (as foreseen by the provider) and **actual use** (as deployed by the users) of the tool/service; additionally, it shows whether it has been **evaluated** or not.

.. image:: ResourceView17.png
    :width: 1200px


.. _Relations:

3. Relations
==============

This tab contains links to the **resources which are related** to the resource being viewed (examples include part-whole relationships, aligned-non aligned versions etc.). 

.. image:: ResourceView9.png
    :width: 1200px

.. attention:: This tab is omitted when there are no relations to other resources.

.. _Access:

4. Access
============

The last tab is the **Access** tab. It contains all the information about the **modes of distribution** (i.e. the forms a resource is accessible, e.g. CD-ROM, via a link from where it can be downloaded, etc.). Each form comes along with **licence terms** and if needed any other information such as an attribution text. 

.. image:: ResourceView10.png
    :width: 1200px

.. attention:: This tab is omitted in case the resource has no content files (see for instance `meta-resources <https://inventory.clarin.gr/search/?meta_resources_initial_page__term=Metaresources>`_).  

.. _Download:

4.1 Download
--------------

If the resource is provided under a licence which allows you to download it, you will see the :guilabel:`Download` button. Simply click on it to get the resource content files. 

.. attention:: Please check which are the prerequisites for a resource to be :ref:`accessible<Accessible>` (and therefore downloadable) in the infrastructure. 

4.2 Process
------------

If the resource has the features that make it compatible with the infrastructure workflows and is therefore :guilabel:`processable`, you will see a :guilabel:`Process` button [#]_. See :ref:`here<Proceed>` an example of processing a bilingual corpus [#]_ .

.. image:: ResourceView11.png
    :width: 1200px

4.3 Use
---------

.. attention:: The :guilabel:`Use` button appears only in metadata records for tools/services!

If the resource is provided under a licence which allows you to :guilabel:`Use` it, you will see a button. See :ref:`here<ServiceUse>` an example of using a tool/service [#]_. 

.. image:: ResourceView12.png
    :width: 1200px


Features
~~~~~~~~~~~

.. attention:: The following section appears **only** in metadata records for **corpora and lexical/conceptual resources**!

At the bottom of the **Access** page, there is a hidden category of metadata called **Features**. Click on the arrow to reveal all the features per corpus part, i.e. the corpus part **size**, the **data format** and the **encoding** (of the text in this case).

.. image:: ResourceView13.png
    :width: 1200px

.. [#] For the time being the metadata record can only be exported in XML format. Soon, other formats will be supported as well.
.. [#] If you are not :ref:`signed in<SignAsUser>`, the button prompts you to do so (:guilabel:`Sign in to process`). After signing in, you are redirected to the resource view page where the :guilabel:`Process` button appears.
.. [#] The corpus used for this example is `A parallel subcorpus collected from the European Constitution (EN-EL) (Moses) <http://hdl.handle.net/11500/ATHENA-0000-0000-23FF-A>`_. 
.. [#] The resource used for this example is the `Annotator of Named Entities GrNE-Tagger <http://hdl.handle.net/11500/ATHENA-0000-0000-23F2-7>`_.