.. _Browse:

#############
Browsing
#############

From the :ref:`inventory home page<LandingPage>` click on the **browse** (or the **search**) button. You will be directed to the central inventory. On the left side of the page you can see the available filters which are explained :ref:`here<Search>`.
In the inventory each resource is provided with a snippet of information. 

.. image:: Snippet.png
    :width: 1200px

On the left there is the logo of the resource's institutional repository. Next, there is the hyperlinked name of the resource which directs to its :ref:`view page<ResourceView>`. Below the name, there is the resource type followed by the first lines of the description. Then there is information on the language, the keywords and the media type. 
On the right side of the page there are tags which provide information on the **processability** [#]_ and the **accessibility** [#]_ of a resource at a glance. Some of the most frequent tags are the following:

* :guilabel:`processable`: a corpus which is compatible with the services of the infrastructure and can be processed [to do so, you must visit the resource view page and click the :ref:`Process<Processable>` button found in the :ref:`Access<Access>` tab].

* :guilabel:`processing service`: a tool which has been integrated as a service and can be used for processing of processable resources [to do so, you must visit the resource view page and click the :ref:`Use<ServiceUse>` button found in the :ref:`Access<Access>` tab].

* :guilabel:`dowloadable`: a resource which can be downloaded, directly through `CLARIN:EL <https://inventory.clarin.gr/>`_ or indirectly through an external link [to do so, you must visit the resource view page and click the :ref:`Download<Download>` button found in the :ref:`Access<Access>` tab].

* :guilabel:`accessible through interface`: a resource which is accessed by an interface available through an external link [to do so, you must visit the resource view page and click the :ref:`Access<Access>` button found in the :ref:`Access<Access>` tab].

Just below the search box on top of the page there are two icons as indicated in the image below. 

.. image:: Presentation.png
    :width: 1200px

They provide two different ways of presentation for the inventory. By default, the LRTs are presented in a list but if you click on the icon on the right they are presented in a table form.

.. image:: Table.png
    :width: 1200px

By clicking on the name of a resource, you can proceed to its :ref:`view page<ResourceView>`. 

.. [#] The following values provide information on the way a resouce is accessible: downloadable, CD-ROM, DVD-R, accessible through interface, accessible through query, bluRay, hard disk, other, unspecified. 

.. [#] The following values provide information on the way a tool/service is accessible: library, plugin, source code, source and executable code, web service, workflow file, docker image, other.