.. _Contact:

#################
Contact & Help
#################

There are three helpdesks to help you with any questions you might have: for `technical and management issues <technical-helpdesk@clarin.gr>`_, `legal issues <legal-helpdesk@clarin.gr>`_, and issues related to `metadata creation and documentation <metadata-helpdesk@clarin.gr>`_.

Links to the helpdesks (as well as FAQs and bibliography) are found at the bottom of each page in the infrastructure.

.. image:: Contact.png
    :width: 1200px
