.. _AboutClarin:

#################
About CLARIN:EL
#################

`CLARIN:EL <https://inventory.clarin.gr/>`_ is a Research Infrastructure for :ref:`Language Resources & Technologies<typesOfResources>` (LRTs); it is the greek part of the European `CLARIN ERIC <https://www.clarin.eu/>`_ Infrastructure. It provides a multitude of **assets related to Language Technology (LT)** for and by **Social Sciences and Humanities** (and beyond), focusing mainly but not exclusively on **Greek LRTs**. The CLARIN:EL Research Infrastructure operates as a distributed network of repositories consisting of:

* the Institutional Repositories (created for each Organisation participating in the CLARIN:EL network) and

* the Hosted Resources Repository (HRR), maintained by ATHENA RC.

Follow the links [#]_ to see the contents of each repository:

* `Athena Research Centre <https://inventory.clarin.gr/search?repository__term=ATHENA%20RC%20Repository>`_,

* `Aristotle University of Thessaloniki <https://inventory.clarin.gr/search?repository__term=Aristotle%20University%20of%20Thessaloniki%20Repository>`_,

* `Athens University of Economics and Business <https://inventory.clarin.gr/search?repository__term=Athens%20University%20of%20Economics%20and%20Business%20Repository>`_,

* `University of the Aegean <https://inventory.clarin.gr/search?repository__term=University%20of%20the%20Aegean%20Repository>`_,

* `National and Kapodistrian University of Athens <https://inventory.clarin.gr/search?repository__term=National%20and%20Kapodistrian%20University%20of%20Athens%20Repository>`_,

* `Centre for the Greek Language <https://inventory.clarin.gr/search?repository__term=Centre%20For%20The%20Greek%20Language%20Repository>`_,

* `Hosted Resources repository <https://inventory.clarin.gr/search?repository__term=Hosted%20Resources%20Repository>`_,

* `University of Crete <https://inventory.clarin.gr/search?repository__term=University%20of%20Crete%20Repository>`_,

* `Ionian University <https://inventory.clarin.gr/search?repository__term=Ionian%20University%20Repository>`_,

* `National Centre of Social Research (EKKE) <https://inventory.clarin.gr/search?repository__term=EKKE%20Repository>`_,

* `National Centre for Scientific Research "Demokritos" (NCSR) <https://inventory.clarin.gr/search?repository__term=NCSR%20%27Demokritos%27%20Repository>`_,

* `Panteion University <https://inventory.clarin.gr/search?repository__term=Panteion%20University%20Repository>`_, and

* University of West Attica.

In the central inventory you will find the complete list of LRTs that have been published in the aforementioned repositories. 

.. tip:: See how :ref:`users<Users>` are connected to their repositories and the whole infrastructure. 

.. [#] When there is no link, the respective organization has not created any resources yet.
