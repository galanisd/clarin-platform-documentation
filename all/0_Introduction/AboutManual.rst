.. _AboutManual:

#########################
How to use this manual 
#########################

This manual [#]_ aims to help you explore and/or use the `CLARIN:EL <https://inventory.clarin.gr/>`_ infrastructure to make your resources available to the **Humanities and Social Sciences** community (and beyond). It is not meant to be read in sequence (although it can be) but to help you find specific information depending on your needs. 
There are chapters with general information on :ref:`basic concepts<RegisterAsUser>`; others describing the :ref:`process<publicationLifecycle>` [#]_ through which a resource comes to life and chapters which will specifically help you:

* to :ref:`browse<Browse>` and :ref:`search<Search>` through the central inventory so as to find resources to :ref:`download<Access>` and :ref:`process<Process>`,

* to create resources via the :ref:`metadata editor<Editor>` [#]_ or by :ref:`uploading XML files<Upload>`, and

* to perform :ref:`actions on resources<Managing>` depending on your role. 

If you are looking for something specific, please, use the search box on the top left side of the navigation bar, below the CLARIN:EL logo. 

.. [#] The current version documents the third official release of the CLARIN:EL infrastructure, launched on May 31st, 2021. More functionalities are continuously added, and this manual keeps on being updated following the evolution of the CLARIN:EL platform.
.. [#] Before you start, please see :ref:`the lifecycle of a resource<publicationLifecycle>` to find out what each type of user needs to do throughout the procedure. To assume a role you must have :ref:`registered <RegisterAsUser>` first. 
.. [#] The terms **metadata editor** and **editor** are used interchangeably throughout the documentation.