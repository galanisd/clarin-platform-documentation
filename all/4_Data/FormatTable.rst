Tables

1. table with FORMATS

+---------------------------+-------------------------------+--------------------------------------------+-----------------------+
|Types of resources         |Categories                     |Recommended formats                         |Acceptable formats     |
+===========================+===============================+============================================+=======================+
|CLARIN:EL processable data |*Monolingual text data*        |Plain Text                                  |                       |
|                           +-------------------------------+--------------------------------------------+-----------------------+
|                           |*Monolingual encoded data*     |XCES-ILSP variant: XML based format         |                       |
|                           |                               |compliant with the XCES model for corpora   |                       |
|                           +-------------------------------+--------------------------------------------+-----------------------+
|                           |*Bi-/Multilingual encoded data*|TMX (XML based format for aligned data),    |                       |
|                           |                               |Moses(text-based format for parallel data)  |                       |
+---------------------------+-------------------------------+--------------------------------------------+-----------------------+
|Textual Data               |*File Formats*                 |Plain Text                                  |PDF, SGML, PostScript, |
|                           +-------------------------------+--------------------------------------------+Rich Text Format(.rtf),|
|                           |*Formatted/Encoded*            |ODT, DOCX, PDF/A, HTML, Latex, Tex, Moses   |Microsoft Word (.doc)  |
+---------------------------+-------------------------------+--------------------------------------------+-----------------------+
|Text Annotation            |*File Formats*                 |XML, XMI, CSV, TSV, RDF (all serialisation  |SGML, Plain Text,      |
|                           |                               |formats RDF/XML, Turtle, Notation3, TriG,   |ELLOGON, Microsoft     |
|                           |                               |N-Triples,N-Quads, JSON-LD, HDT), JSON      |Excel (.xls, .xlsx)    |
|                           +-------------------------------+--------------------------------------------+-----------------------+
|                           |*Models*                       |XCES for corpora & structural annotation,   |                       |
|                           |                               |TEI for structural & linguistic annotation, |                       |
|                           |                               |GrAF linguistic annotation, TMX for aligned,|                       |
|                           |                               |GATE linguistic annotation, CoNLL family (  |                       |
|                           |                               |CoNLL-U, CoNLL-2000, CoNLL-2002, CoNLL-2003,|                       |
|                           |                               |CoNLL-2006, CoNLL-2008, CoNLL-2009,         |                       |
|                           |                               |CoNLL-2012) for linguistic annotation, NIF  |                       |
|                           |                               |linguistic annotation for RDF data, WARC for|                       |
|                           |                               |web crawled data                            |                       |
+---------------------------+-------------------------------+--------------------------------------------+-----------------------+
|Language Description       |*ML Model*                     |H5, ProtoBuf, ONNX, PMML, Pickle, MLeap,    |                       |
|                           |                               |YAML, JSON                                  |                       |
|                           +-------------------------------+--------------------------------------------+-----------------------+
|                           |*N-gram model*                 |ARPA                                        |                       |
+---------------------------+-------------------------------+--------------------------------------------+-----------------------+
|Lexical/Conceptual Resource|*File Formats*                 |XML, CSV, TSV, RDF (RDF/XML, Turtle, TriG,  |Microsoft Excel (.xlsx,|
|                           |                               |Notation3, N-Triples, N-Quads, JSON-LD,     |.xls),Plain Text, SQL  |
|                           |                               |HDT), OWL                                   |                       |
|                           +-------------------------------+--------------------------------------------+-----------------------+
|                           |*Models*                       |LMF for lexica, OWL for ontologies, SKOS for|                       |
|                           |                               |thesauri, OntoLex-Lemon for lexica, TBX for |                       |
|                           |                               |terminological data                         |                       |
+---------------------------+-------------------------------+--------------------------------------------+-----------------------+
|Image data                 |*All images*                   |TIFF, SVG, JPEG 2000, PNG, GIF              |JPEG, BMP, Photoshop,  |
|                           |                               |                                            |NifTi, FlashPix, PDF   |
|                           +-------------------------------+--------------------------------------------+                       |
|                           |*Scanned images*               |PDF/A                                       |                       |
+---------------------------+-------------------------------+--------------------------------------------+-----------------------+
|Audio data                 |                               |WAV, AIFF, FLAC                             |MP3, MPEG, Windows,    |
|                           |                               |                                            |Media Audio            |
+---------------------------+-------------------------------+--------------------------------------------+-----------------------+
|Video data                 |                               |AVI                                         |MPEG-4, RealNetworks   |
|                           |                               |                                            |'Real Video', Windows  |
|                           |                               |                                            |Media Video, Flash     |
|                           |                               |                                            |Video, QuickTime       |
|                           |                               |                                            |Video                  |
+---------------------------+-------------------------------+--------------------------------------------+-----------------------+

2. Table with metadata

+--------------------+------------------------------------------------------------------------------+
| **value**          | **example**                                                                  |
+====================+==============================================================================+
|a single word       |``<ms:keyword xml:lang="en">`` alignment ``</ms:keyword>``                    |
+--------------------+------------------------------------------------------------------------------+
|a phrase            |``<ms:categoryLabel xml:lang="en">`` Political Science ``</ms:categoryLabel>``|
+--------------------+------------------------------------------------------------------------------+
|multiple paragraphs |*See examples in full XML descriptions*                                       |
+--------------------+------------------------------------------------------------------------------+
|a date              |``<ms:creationStartDate>`` 2005-10-01 ``</ms:creationStartDate>``             |
+--------------------+------------------------------------------------------------------------------+
|a number            |``<ms:amount>`` 100000.0 ``</ms:amount>``                                     |
+--------------------+------------------------------------------------------------------------------+
|a URL               |``<ms:website>`` http://www.ilsp.gr/ ``</ms:website>``                        |
+--------------------+------------------------------------------------------------------------------+
|an email            |``<ms:email>`` name@athenarc.gr ``</ms:email>``                               |
+--------------------+------------------------------------------------------------------------------+
|other metadata with |``<ms:additionalInfo>``                                                       |
|their values        |``<ms:email>`` name@athenarc.gr ``</ms:email>``                               |
|                    |``</ms:additionalInfo>``                                                      |
+--------------------+------------------------------------------------------------------------------+


OR


+--------------------+------------------------------------------------------------------+-------------------------------+
| **value**          | **example**                                                      | **explanation**               |
+====================+==================================================================+===============================+
|a single word       |``<ms:keyword xml:lang="en">`` alignment ``</ms:keyword>``        |*Aligmnent* is a keyword       |
+--------------------+------------------------------------------------------------------+-------------------------------+
|a phrase            |                                                                  |                               |
+--------------------+------------------------------------------------------------------+-------------------------------+
|multiple paragraphs |                                                                  |                               |
+--------------------+------------------------------------------------------------------+-------------------------------+
|a date              |``<ms:creationStartDate>`` 2005-10-01 ``</ms:creationStartDate>`` |*October 1st 2005* is the      |
|                    |                                                                  |creation start date            |
+--------------------+------------------------------------------------------------------+-------------------------------+
|a number            |``<ms:amount>`` 100000.0 ``</ms:amount>``                         |The amount is *100000*         |
+--------------------+------------------------------------------------------------------+-------------------------------+
|a URL               |``<ms:website>`` http://www.ilsp.gr/ ``</ms:website>``            |The website is *www.ilsp.gr*   |
+--------------------+------------------------------------------------------------------+-------------------------------+
|an email            |``<ms:email>`` name@athenarc.gr ``</ms:email>``                   |The email is *name@athenarc.gr*|
+--------------------+------------------------------------------------------------------+-------------------------------+
|other metadata with |``<ms:additionalInfo>``                                           |The ``email`` is a metadatum   |
|their values        |``<ms:email>`` name@athenarc.gr ``</ms:email>``                   |which is the value of the      |
|                    |``</ms:additionalInfo>``                                          |``additional info``            |
+--------------------+------------------------------------------------------------------+-------------------------------+
