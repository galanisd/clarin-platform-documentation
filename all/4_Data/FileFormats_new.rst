.. _FileFormats_new:

#############################
Recommended File Formats
#############################

Guidance on selecting file formats for long-term accessibility and interoperability
******************************************************************************************

Please refer to the information provided `here <https://standards.clarin.eu/sis/views/view-centre.xq?id=CLARIN:EL>`_ for the file formats which are recommended for depositing in CLARIN:EL.