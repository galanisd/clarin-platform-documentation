.. _Fair:

##################
FAIR principles
##################

.. raw:: html

	<p style="border:2px; border-style:solid; border-color:#c4bfb3; border-radius: 10px; box-shadow: 5px 5px 3px 1px #999; padding: 0.5em; font-family:calibri light; font-size:17px;"><i><b>In this chapter:</b> findability, accessibility, interoperability and reuse of digital assets.</i><br></p>

The `CLARIN:EL infrastructure <https://inventory.clarin.gr/>`_ and :ref:`metadata schema <FullSchema>` support the `FAIR principles <https://www.go-fair.org/fair-principles/>`_ : **Findability**, **Accessibility**, **Interoperability** and **Reuse** of digital assets. This section provides an overview of the FAIR principles. For detailed information, please, visit the `GoFair website <https://www.go-fair.org/>`_ where each one of the principles is further subdivided and analysed.

.. _Findability:

Findability
************

First data should be found. One of the ways to trace a digital object regardless of changes to its location on the internet is the **persistent identifier (PID)**. A PID is a string uniquely identifying a digital object. In `CLARIN:EL <https://inventory.clarin.gr/>`_ each resource is given a PID upon being published and can be retrieved with it even if the resource has been removed from the central inventory. For example, the *Sentiment Analysis Tool* has been unpublished but by using its PID (http://hdl.handle.net/11500/DEMOKRITOS-0000-0000-24A2-0) the user is directed to the resource view page where a tombstone indicates that **this resource is temporarily unavailable**. 

.. image:: Tombstone.png
    :width: 1200px

Another way to find data is via their **metadata descriptions**. The more (and accurate) metadata provided, the merrier. On the GoFair website the importance of metadata is pointed out with a simple rule of thumb: *"you should never say ‘this metadata isn’t useful’; be generous and provide it anyway!"*

.. _Accessibility:

Accessibility
**************

Once the data are found, the user should know how to access them: *"anyone with a computer and an internet connection can access at least the metadata"* [#]_. Accessibility in this context is the ability to retrieve data and metadata without specialised or proprietary  tools or communication methods. However, accessibility is not condition free. Authentication and/or authorisation could be required where necessary. In `CLARIN:EL <https://inventory.clarin.gr/>`_ authentication and authorisation are required when a user wants access to specific rights (as a :ref:`curator <Curator>`, :ref:`validator <Validator>` or :ref:`supervisor <Supervisor>`) or when one wants access to processing services. In these cases the user has to :ref:`register <Register>`/:ref:`sign in <SignAsUser>` first; browsing, viewing and exporting metadata records as well as downloading resources are available to non registered users.

Accessibility is also assured when metadata are available even when the data are not. Besides the tombstone mentioned for  resources which have been unpublished, `CLARIN:EL <https://inventory.clarin.gr/>`_ has also **for info** resources  either because data are under process and not ready to be published or because legal clearance is pending. These metadata records still provide all the necessary information about the upcoming data and offer contact details. 

.. image:: ForInfo.png
    :width: 1200px

.. _Interoperability:

Interoperability
******************

Interoperability concerns both data and metadata and their perception from humans and computers. In simple words, exchange and interpretation of data should be a seamless effort between humans or machines. To allow for readibility without the need for additional software (algorithms, translators, mappings) commonly accepted "controlled vocabularies, ontologies, thesauri and a good data model (*a well-defined framework to describe and structure (meta)data*) should be used" [#]_.

Towards this end, qualified references between resources (cross-references which explicitely state the connection of resources) are also needed. In `CLARIN:EL <https://inventory.clarin.gr/>`_ the metadata schema has forseen for such links, an example of which is presented in the image below. The `OROSSIMO Corpus - Economics <http://hdl.handle.net/11500/ATHENA-0000-0000-240D-A>`_ is, as indicated in the relations part of the resource view page, part of the `OROSSIMO Corpus <http://hdl.handle.net/11500/ATHENA-0000-0000-2410-5>`_ and has outcome the `Orossimo Terminological Resource - Economics <http://hdl.handle.net/11500/ATHENA-0000-0000-4B53-F>`_.

.. image:: Relations.png
    :width: 1200px

In addition, all resources are cited with their PID included.

.. image:: Citation.png
    :width: 1200px

.. _Reuse:

Reuse
**********

In order to be able to reuse data, the metadata used should richly describe all aspects related to the data generation. The term **plurality** is used "to indicate that the metadata author should be as generous as possible in providing metadata, even including information that may seem irrelevant" [#]_.

For the same reason, the licensing status of data should be clear. The CLARIN:EL :ref:`metadata schema <FullSchema>` has multiple metadata elements referring to all legal aspects (licence terms, URL, conditions of use etc.) of a resource. These appear as fields in the metadata editor [#]_ as shown in the image below: 

.. image:: Licencing.png
    :width: 1200px

.. [#] `FAIR Principles > A1.1: The protocol is open, free and universally implementable <https://www.go-fair.org/fair-principles/a1-1-protocol-open-free-universally-implementable/>`_.
.. [#] `FAIR Principles >  I1: (Meta)data use a formal, accessible, shared, and broadly applicable language for knowledge representation <https://www.go-fair.org/fair-principles/i1-metadata-use-formal-accessible-shared-broadly-applicable-language-knowledge-representation/>`_.
.. [#] `FAIR Principles > R1: (Meta)data are richly described with a plurality of accurate and relevant attributes <https://www.go-fair.org/fair-principles/r1-metadata-richly-described-plurality-accurate-relevant-attributes/>`_
.. [#] Henceforth **editor**.

