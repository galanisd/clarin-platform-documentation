.. _FullSchema:

##################################
Full schema documentation
##################################

You can browse the full schema documentation here:

- `Metadata record (Base item) <https://clarin-platform-documentation.readthedocs.io/en/latest/Documentation/CLARIN-SHARE_schema.html#MetadataRecord>`_

- `Language Resource <https://clarin-platform-documentation.readthedocs.io/en/latest/Documentation/CLARIN-SHARE_schema.html#LanguageResource>`_
	- `Tool/Service <https://clarin-platform-documentation.readthedocs.io/en/latest/Documentation/CLARIN-SHARE_schema.html#ToolService>`_
	- `Corpus <https://clarin-platform-documentation.readthedocs.io/en/latest/Documentation/CLARIN-SHARE_schema.html#Corpus>`_
	- `Language description <https://clarin-platform-documentation.readthedocs.io/en/latest/Documentation/CLARIN-SHARE_schema.html#LanguageDescription>`_
	- `Lexical/Conceptual resource <https://clarin-platform-documentation.readthedocs.io/en/latest/Documentation/CLARIN-SHARE_schema.html#LexicalConceptualResource>`_


The metadata  building blocks (either attributes or elements) are all listed alphabetically.

.. image:: MetadataSchema.png
    :width: 1200px

If you want to find out more about an element, simply click on it and you will be transferred to its full description. In the following example two elements are presented: **download location** and **duration of audio**.

.. image:: Elements.png
    :width: 1200px
