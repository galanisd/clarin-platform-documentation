.. _Mandatory:

#############################
Mandatory metadata 
#############################

.. _allM:

The :ref:`metadata schema<FullSchema>` has elements of various degrees of optionality, i.e. how necessary they are considered for the description of a resource. Regardless of the mode you choose to create a resource (via the :ref:`metadata editor<Editor>` [#]_ or by :ref:`XML description upload<Upload>`), the **mandatory** metadata must be filled in. Otherwise, the record will not be saved or the XML desctiption will not be imported. The following sections contain images which provide an overview of these metadata per resource type and they are briefly explained. The elements which are indicated by an **asterisk (*)** are **mandatory upon condition**; this means that their necessity depends on the values of other elements provided by the user. The images replicate the editor (with sections horizontally and tabs vertically) so that you can easily track each element. In the editor, all elements, mandatory or not, are explained by definitions and examples.

Common Mandatory Elements
****************************
.. tip:: Some elements are mandatory for all resources!

To **describe** a resource efficiently you need to **name** it and provide a description with a few words about it. To facilitate reading for the users and make your description more appealing, make use of the rich editor functionalities (styling, word formatting, hyperlinking, alignment etc.) indicated in the image below.

.. image:: RichEd.png
    :width: 1200px
 
Next, indicate the resource **version**; if no version number is provided, the system will automatically add number v1.0.0. In addition, one or more **keywords** are asked for the resource and an **email** or a **landing page** for anyone who wishes to have **additional information** about it.

These metadata are found in the section :guilabel:`Language Resource/Technology` in the :guilabel:`Identity` tab.   

You also have to describe independently each **distributable** form of the resource (i.e. all the ways the user can obtain it, either in a compact form, as a CD-ROM or from access points such as a **distribution/ download/ access location**) and you must always select the **licence** and **licence terms** under which the resource is made available (see `here <https://www.clarin.gr/en/support/legal>`_ the Recommended licensing scheme for Language Resources).

These metadata are found in the section :guilabel:`Distribution` in the :guilabel:`Technical` tab.

Mandatory Elements per resource type
**************************************

.. _corpusM:

A Corpus at a glance
=====================

.. image:: CorpusM.png
    :width: 1200px

Beyond the :ref:`common mandatory elements<allM>`, for a corpus to be described information is also needed on the **corpus subclass** (if it is raw or annotated, for example) and whether **personal or sensitive** data are included. If this is the case, you must say whether they have been **anonymized**.
Your corpus could also have various **media parts** (namely: text, video, audio, image or textNumerical parts). Each of these parts must be described separately. For instance, if you have a text corpus of transcribed discussions along with the audio files you must provide information on both media indicating the **size** and **data format** of each one. 

.. tip:: See :ref:`here <CorporaXML>` examples of XML metadata descriptions for corpora. 

.. _toolM:

A Tool at a glance
========================

.. image:: ToolM.png
    :width: 1200px

Beyond the :ref:`common mandatory elements<allM>`, for a tool to be described you have to indicate its **function** (i.e. the task it performs) and also to choose if it is **language dependent** or not. If this is the case, you must also select one or more **languages** the tool can handle. The **resource type** the tool takes as **input** (e.g. corpus) must also be defined. 

.. tip:: See :ref:`here <ToolXML>` examples of XML metadata descriptions for tools. 

.. _LCRM:

A Lexical/Conceptual Resource (LCR) at a glance
==================================================

.. image:: LCRM.png
    :width: 1200px

Beyond the :ref:`common mandatory elements<allM>`, for a Lexical/conceptual (LCR) resource to be described, information is also needed on the **encoding level** (the linguistic level of analysis of the LCR, e.g. morphology, phonology, semantics, etc.) and whether **personal or sensitive** data are included. If this is the case, you must say whether they have been **anonymized**.
Your LCR could also have various **media parts** (namely: text, video, audio or image parts). Each of these parts must be described separately. For instance, if you have a digital lexicon with definitions in text and aufio files for the pronunciation you must provide information on both media indicating the **size** and **data format** of each one. 

.. tip:: See :ref:`here <LCRXML>` examples of XML metadata descriptions for lexical/conceptual resources.

.. _LDM:

A Language Description at a glance
=======================================

.. image:: LDM.png
    :width: 1200px

Beyond the :ref:`common mandatory elements<allM>`, for a Language Description to be described information is also needed on the **Language Description subclass** (whether it is a **grammar**, an **ML model** or a **n-gram model**) and whether **personal or sensitive** data are included. If this is the case, you must say whether they have been **anonymized**.
Your Language Description could also have various **media parts** (namely: text, video, audio or image). Each of these parts must be described separately. For instance, if you have a text grammar  along with the video files showcasing examples you must provide information on both media indicating the **size** and **data format** of each one.

.. tip:: See :ref:`here<LDXML>` examples of XML metadata descriptions for language descriptions.

.. attention:: Although only the mandatory metadata are the required in order to create a record, an LRT description is more complete if the **recommended** metadata are provided as well.

.. [#] Henceforth **editor**.