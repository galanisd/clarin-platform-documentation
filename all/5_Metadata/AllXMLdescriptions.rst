.. _AllXMLdescriptions:

############################
XML metadata descriptions
############################

.. raw:: html

	<p style="border:2px; border-style:solid; border-color:#c4bfb3; border-radius: 10px; box-shadow: 5px 5px 3px 1px #999; padding: 0.5em; font-family:calibri light; font-size:17px;"><i><b>In this chapter:</b> full XML descriptions from the CLARIN:EL infrastructure.</i><br></p>

The XML metadata descriptions presented here have been exported from the `CLARIN:EL infrastructure <https://inventory.clarin.gr/>`_ and describe actual resources. Click on the buttton with the resource name to reveal its XML description. In all files the names, surnames and emails of creators, curators and contacts have been anonymized.

.. _CorporaXML:

1. Corpora
******************

.. _GreekParla:

Monolingual corpus #1
=======================

.. raw:: html

    <button class="btn btn-example-data" onclick="$('#monoCorpus').toggle(300)">Greek Parliament Plenary Sessions</button><div id="monoCorpus" style="display: none;">

.. code-block:: xml

	<?xml version="1.0" encoding="utf-8"?>
	<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://inventory.clarin.gr/metadata-schema/CLARIN-SHARE.xsd">
    <ms:metadataCreationDate>2019-12-17</ms:metadataCreationDate>
    <ms:metadataLastDateUpdated>2021-11-24</ms:metadataLastDateUpdated>
    <ms:metadataCurator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCurator>
    <ms:compliesWith>http://w3id.org/meta-share/meta-share/CLARIN-SHARE</ms:compliesWith>
    <ms:metadataCreator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCreator>
    <ms:sourceOfMetadataRecord>
        <ms:repositoryName xml:lang="el">Αποθετήριο ΕΚ ΑΘΗΝΑ</ms:repositoryName>
        <ms:repositoryName xml:lang="en">ATHENA RC Repository</ms:repositoryName>
        <ms:repositoryURL>http://inventory.clarin.gr</ms:repositoryURL>
    </ms:sourceOfMetadataRecord>
    <ms:DescribedEntity>
        <ms:LanguageResource>
            <ms:entityType>LanguageResource</ms:entityType>
            <ms:resourceName xml:lang="el">Πρακτικά της Ολομέλειας του Ελληνικού Κοινοβουλίου
                (1989-2019)</ms:resourceName>
            <ms:resourceName xml:lang="en">Greek Parliament Plenary Sessions
                (1989-2019)</ms:resourceName>
            <ms:description xml:lang="el">Σώμα κειμένων με τα πρακτικά των συνεδριάσεων της
                Ολομέλειας του Ελληνικού Κοινοβουλίου τα τελευταία 30 χρόνια (περισσότερες από
                1.000.000 ομιλίες). Το παρόν σώμα κειμένων περιλαμβάνει όλα τα πρακτικά σε μορφή
                txt. Για ευκολότερη επεξεργασία έχουν δημιουργηθεί και μικρότερα υποσύνολα, με
                ανώτατο μέγεθος συμπιεσμένων αρχείων τα 40 Mb ανά υποσύνολο, που ανταποκρίνονται
                στις εκάστοτε βουλευτικές περιόδους.</ms:description>
            <ms:description xml:lang="en">This is a collection of the raw minutes of the Greek
                Parliament plenary sessions of the last 30 years (more than 1.000.000 speeches). The
                existing corpus has all raw data in txt format. In order to make the resource more
                processable, we have also split it into smaller subcorpora, with a maximum
                compressed folder size of 40 Mb per subcorpus. The created subcorpora are
                thematically organized per Greek parliamentary terms.</ms:description>
            <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                >http://hdl.handle.net/11500/ATHENA-0000-0000-5D62-A</ms:LRIdentifier>
            <ms:version>1.0.0 (automatically assigned)</ms:version>
            <ms:additionalInfo>
                <ms:email>person@ilsp.gr</ms:email>
            </ms:additionalInfo>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:citationText xml:lang="el">Πρακτικά της Ολομέλειας του Ελληνικού Κοινοβουλίου
                (1989-2019) (2019). Version 1.0.0 (automatically assigned). [Dataset (Text corpus)].
                CLARIN:EL. http://hdl.handle.net/11500/ATHENA-0000-0000-5D62-A</ms:citationText>
            <ms:citationText xml:lang="en">Greek Parliament Plenary Sessions (1989-2019) (2019).
                Version 1.0.0 (automatically assigned). [Dataset (Text corpus)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-5D62-A</ms:citationText>
            <ms:keyword xml:lang="en">monolingual, political science</ms:keyword>
            <ms:resourceProvider>
                <ms:Organization>
                    <ms:actorType>Organization</ms:actorType>
                    <ms:organizationName xml:lang="el">Ινστιτούτο Επεξεργασίας του
                        Λόγου</ms:organizationName>
                    <ms:organizationName xml:lang="en">Institute for Language and Speech
                        Processing</ms:organizationName>
                    <ms:website>http://www.ilsp.gr/</ms:website>
                </ms:Organization>
            </ms:resourceProvider>
            <ms:LRSubclass>
                <ms:Corpus>
                    <ms:lrType>Corpus</ms:lrType>
                    <ms:corpusSubclass>http://w3id.org/meta-share/meta-share/rawCorpus</ms:corpusSubclass>
                    <ms:CorpusMediaPart>
                        <ms:CorpusTextPart>
                            <ms:corpusMediaType>CorpusTextPart</ms:corpusMediaType>
                            <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
                            <ms:lingualityType>http://w3id.org/meta-share/meta-share/monolingual</ms:lingualityType>
                            <ms:language>
                                <ms:languageTag>el</ms:languageTag>
                                <ms:languageId>el</ms:languageId>
                            </ms:language>
                        </ms:CorpusTextPart>
                    </ms:CorpusMediaPart>
                    <ms:DatasetDistribution>
                        <ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
                        <ms:downloadLocation>http://www.hiddenLocation.org</ms:downloadLocation>
                        <ms:distributionTextFeature>
                            <ms:size>
                                <ms:amount>5079.0</ms:amount>
                                <ms:sizeUnit>http://w3id.org/meta-share/meta-share/file</ms:sizeUnit>
                            </ms:size>
                            <ms:dataFormat>http://w3id.org/meta-share/omtd-share/Text</ms:dataFormat>
                            <ms:characterEncoding>http://w3id.org/meta-share/meta-share/UTF-8</ms:characterEncoding>
                        </ms:distributionTextFeature>
                        <ms:licenceTerms>
                            <ms:licenceTermsName xml:lang="en">Creative Commons Attribution 4.0
                                International</ms:licenceTermsName>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by/4.0/legalcode</ms:licenceTermsURL>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by/4.0/</ms:licenceTermsURL>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/attribution</ms:conditionOfUse>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsDirectAccess</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsProcessing</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/public</ms:licenceCategory>
                            <ms:LicenceIdentifier
                                ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX"
                                >CC-BY-4.0</ms:LicenceIdentifier>
                        </ms:licenceTerms>
                        <ms:attributionText xml:lang="el">Πρακτικά της Ολομέλειας του Ελληνικού
                            Κοινοβουλίου (1989-2019). Άδεια: Creative Commons Attribution 4.0
                            International (https://creativecommons.org/licenses/by/4.0/legalcode,
                            https://creativecommons.org/licenses/by/4.0/). Πηγή:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-5D62-A
                            (CLARIN:EL)</ms:attributionText>
                        <ms:attributionText xml:lang="en">Greek Parliament Plenary Sessions
                            (1989-2019) used under Creative Commons Attribution 4.0 International
                            (https://creativecommons.org/licenses/by/4.0/legalcode,
                            https://creativecommons.org/licenses/by/4.0/). Source:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-5D62-A
                            (CLARIN:EL)</ms:attributionText>
                    </ms:DatasetDistribution>
                    <ms:personalDataIncluded>false</ms:personalDataIncluded>
                    <ms:sensitiveDataIncluded>false</ms:sensitiveDataIncluded>
                </ms:Corpus>
            </ms:LRSubclass>
        </ms:LanguageResource>
    </ms:DescribedEntity>
	</ms:MetadataRecord>

.. _GoldenCorpus:

Monolingual corpus #2
=======================

.. raw:: html

    <button class="btn btn-example-data" onclick="$('#monolingualCorpus').toggle(300)">Golden Part of Speech Tagged Corpus</button>
    <div id="monolingualCorpus" style="display: none;">

.. code-block:: xml

	<?xml version="1.0" encoding="utf-8"?>
		<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://inventory.clarin.gr/metadata-schema/CLARIN-SHARE.xsd">
			<ms:metadataCreationDate>2021-02-25</ms:metadataCreationDate>
			<ms:metadataLastDateUpdated>2021-05-28</ms:metadataLastDateUpdated>
			<ms:metadataCurator>
				<ms:actorType>Person</ms:actorType>
				<ms:surname xml:lang="en">Person_Surname</ms:surname>
				<ms:givenName xml:lang="en">Person_Name</ms:givenName>
			</ms:metadataCurator>
			<ms:compliesWith>http://w3id.org/meta-share/meta-share/CLARIN-SHARE</ms:compliesWith>
			<ms:metadataCreator>
				<ms:actorType>Person</ms:actorType>
				<ms:surname xml:lang="en">Person_Surname</ms:surname>
				<ms:givenName xml:lang="en">Person_Name</ms:givenName>
			</ms:metadataCreator>
			<ms:sourceOfMetadataRecord>
				<ms:repositoryName xml:lang="el">Αποθετήριο ΕΚ ΑΘΗΝΑ</ms:repositoryName>
				<ms:repositoryName xml:lang="en">ATHENA RC Repository</ms:repositoryName>
				<ms:repositoryURL>http://inventory.clarin.gr</ms:repositoryURL>
			</ms:sourceOfMetadataRecord>
			<ms:DescribedEntity>
				<ms:LanguageResource>
					<ms:entityType>LanguageResource</ms:entityType>
            <ms:resourceName xml:lang="en">Golden Part of Speech Tagged Corpus</ms:resourceName>
            <ms:description xml:lang="el">Το Golden Part of Speech Tagged Corpus είναι ένα σύνολο
                δεδομένων που αποτελείται από κείμενα επιλεγμένα από το σύνολο των κειμένων του
                ΕΘΕΓ, και το οποίο είναι μεγέθους 100.000 λέξεων. Τα κείμενα συλλέχθηκαν από το
                διαδίκτυο με τεχνικές διάσχισης σημασιολογικού ιστού (web crawling), με τα εξής
                κριτήρια: κείμενα αποκλειστικά με ελεύθερη διάθεση (CC0 4.0) ή με αναφορά
                δημιουργού (CC BY 4.0) και ευρεία ποικιλία πηγών και θεμάτων. Η διαδικασία που
                ακολουθήθηκε για την υλοποίηση αυτού του σώματος δεδομένων περιλαμβάνει τα ακόλουθα
                βήματα: - Καθαρισμό από άχρηστα στοιχεία (boilerplate material) - Διόρθωση
                ορθογραφικών λαθών με το χέρι - Αυτόματη μορφοσυντακτική επισημείωση με χρήση του
                αυτόματου μορφολογικού επισημειωτή του ΙΕΛ/ΕΚ ΑΘΗΝΑ (ILSP Feature-based multi-tiered
                POS Tagger), με την οποία για κάθε λέξη του σώματος δεδομένων αποδόθηκε ο
                γραμματικός της χαρακτηρισμός και το λήμμα στο οποίο ανήκει - Έλεγχος και διόρθωση
                των αποτελεσμάτων της αυτόματης επισημείωσης, επίσης με το χέρι. 
                Το Golden Corpus XML διατίθεται σε μορφή XML, ως αρχείο το οποίο περιλαμβάνει όλα τα
                κείμενα. Η δομή κάθε κειμένου περιλαμβάνει αρχικά ορισμένα μεταδεδομένα για το
                κείμενο και μετά το ίδιο το κείμενο, που είναι δομημένο
                σε επισημειωμένες παραγράφους, προτάσεις και λέξεις. Κάθε λέξη συνοδεύεται από
                πληροφορίες για το λήμμα της, τον γραμματικό χαρακτηρισμό της και την θέση της μέσα
                στην πρόταση με βάση τους χαρακτήρες έναρξης και λήξης. Επιλέχθηκε ως μορφή
                διάθεσης η XML ώστε το σύνολο δεδομένων να μπορεί να χρησιμοποιηθεί σε διαφορετικά
                περιβάλλοντα, ανεξαρτήτως  λειτουργικού συστήματος.</ms:description>
            <ms:description xml:lang="en">The Golden Part-of-Speech Tagged Corpus is a subset of the
                Hellenic National Corpus (HNC), the size of which is 100.000 words; it consists of
                selected texts from a variety of sources covering various domains. These texts have
                been crawled from the web and are licensed under either CC0 4.0 or CC BY 4.0. The
                corpus underwent the following stages: • cleaning and removal of
                boilerplate material, • manual correction of typos and spelling mistakes, •
                automatic lemmatization and part-of-speech tagging for each word, using the ILSP
                Feature-based multi-tiered POS Tagger, and • manual correction of the ILSP
                Feature-based multi-tiered POS Tagger results. The Golden Part of Speech Tagged
                Corpus is available as a single XML file, containing all texts in the following
                structure: first, some metadata about the text and then the text itself with
                annotation at the level of paragraphs, sentences and words. Each word comes with
                information on its lemma, POS and its boundaries (beginning and end). XML was chosen
                as the most appropriate format as it can be used in various environments, regardless
                of the operating system in use.</ms:description>
            <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                >http://hdl.handle.net/11500/ATHENA-0000-0000-5E7D-C</ms:LRIdentifier>
            <ms:version>1</ms:version>
            <ms:additionalInfo>
                <ms:email>person@ilsp.athena-innovation.gr</ms:email>
            </ms:additionalInfo>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:citationText xml:lang="el">Ινστιτούτο Επεξεργασίας του Λόγου - Ερευνητικό Κέντρο
                Αθηνά (2021). Golden Part of Speech Tagged Corpus. Version 1. [Dataset (Text
                corpus)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-5E7D-C</ms:citationText>
            <ms:citationText xml:lang="en">Institute for Language and Speech Processing - Athena
                Research Center (2021). Golden Part of Speech Tagged Corpus. Version 1. [Dataset
                (Text corpus)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-5E7D-C</ms:citationText>
            <ms:keyword xml:lang="en">monolingual</ms:keyword>
            <ms:keyword xml:lang="en">morphosyntacticAnnotation-posTagging</ms:keyword>
            <ms:resourceCreator>
                <ms:Organization>
                    <ms:actorType>Organization</ms:actorType>
                    <ms:organizationName xml:lang="el">Ινστιτούτο Επεξεργασίας του
                        Λόγου</ms:organizationName>
                    <ms:organizationName xml:lang="en">Institute for Language and Speech
                        Processing</ms:organizationName>
                    <ms:website>http://www.ilsp.gr/</ms:website>
                </ms:Organization>
            </ms:resourceCreator>
            <ms:isPartOf>
                <ms:resourceName xml:lang="el">Ελληνικός Θησαυρός της Ελληνικής
                    Γλώσσας</ms:resourceName>
                <ms:resourceName xml:lang="en">Hellenic National Corpus</ms:resourceName>
                <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                    >http://hdl.handle.net/11500/ATHENA-0000-0000-23E2-9</ms:LRIdentifier>
                <ms:version>3.0</ms:version>
            </ms:isPartOf>
            <ms:LRSubclass>
                <ms:Corpus>
                    <ms:lrType>Corpus</ms:lrType>
                    <ms:corpusSubclass>http://w3id.org/meta-share/meta-share/annotatedCorpus</ms:corpusSubclass>
                    <ms:CorpusMediaPart>
                        <ms:CorpusTextPart>
                            <ms:corpusMediaType>CorpusTextPart</ms:corpusMediaType>
                            <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
                            <ms:lingualityType>http://w3id.org/meta-share/meta-share/monolingual</ms:lingualityType>
                            <ms:language>
                                <ms:languageTag>el</ms:languageTag>
                                <ms:languageId>el</ms:languageId>
                            </ms:language>
                            <ms:annotation>
                                <ms:annotationType>http://w3id.org/meta-share/omtd-share/PartOfSpeech</ms:annotationType>
                                <ms:isAnnotatedBy>
                                    <ms:resourceName xml:lang="en">ILSP Feature-based multi-tiered
                                        POS Tagger</ms:resourceName>
                                    <ms:LRIdentifier
                                        ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                                        >http://hdl.handle.net/11500/ATHENA-0000-0000-23E8-3</ms:LRIdentifier>
                                    <ms:version>1</ms:version>
                                </ms:isAnnotatedBy>
                            </ms:annotation>
                            <ms:annotation>
                                <ms:annotationType>http://w3id.org/meta-share/omtd-share/StructuralAnnotationType</ms:annotationType>
                                <ms:segmentationLevel>http://w3id.org/meta-share/meta-share/sentence</ms:segmentationLevel>
                                <ms:segmentationLevel>http://w3id.org/meta-share/meta-share/token1</ms:segmentationLevel>
                            </ms:annotation>
                            <ms:annotation>
                                <ms:annotationType>http://w3id.org/meta-share/omtd-share/Lemma</ms:annotationType>
                            </ms:annotation>
                        </ms:CorpusTextPart>
                    </ms:CorpusMediaPart>
                    <ms:DatasetDistribution>
                        <ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
                        <ms:downloadLocation>http://www.hiddenLocation.org</ms:downloadLocation>
                        <ms:accessLocation>http://fixme.com</ms:accessLocation>
                        <ms:distributionTextFeature>
                            <ms:size>
                                <ms:amount>100000.0</ms:amount>
                                <ms:sizeUnit>http://w3id.org/meta-share/meta-share/word3</ms:sizeUnit>
                            </ms:size>
                            <ms:dataFormat>http://w3id.org/meta-share/omtd-share/Xml</ms:dataFormat>
                            <ms:characterEncoding>http://w3id.org/meta-share/meta-share/UTF-8</ms:characterEncoding>
                        </ms:distributionTextFeature>
                        <ms:licenceTerms>
                            <ms:licenceTermsName xml:lang="en">Creative Commons Attribution 4.0
                                International</ms:licenceTermsName>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by/4.0/legalcode</ms:licenceTermsURL>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by/4.0/</ms:licenceTermsURL>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/attribution</ms:conditionOfUse>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsDirectAccess</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsProcessing</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/public</ms:licenceCategory>
                            <ms:LicenceIdentifier
                                ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX"
                                >CC-BY-4.0</ms:LicenceIdentifier>
                        </ms:licenceTerms>
                        <ms:attributionText xml:lang="el">Golden Part of Speech Tagged Corpus.
                            Δημιουργός: Ινστιτούτο Επεξεργασίας του Λόγου - Ερευνητικό Κέντρο Αθηνά.
                            Άδεια: Creative Commons Attribution 4.0 International
                            (https://creativecommons.org/licenses/by/4.0/legalcode,
                            https://creativecommons.org/licenses/by/4.0/). Πηγή:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-5E7D-C
                            (CLARIN:EL)</ms:attributionText>
                        <ms:attributionText xml:lang="en">Golden Part of Speech Tagged Corpus by
                            Institute for Language and Speech Processing - Athena Research Center
                            used under Creative Commons Attribution 4.0 International
                            (https://creativecommons.org/licenses/by/4.0/legalcode,
                            https://creativecommons.org/licenses/by/4.0/). Source:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-5E7D-C
                            (CLARIN:EL)</ms:attributionText>
                    </ms:DatasetDistribution>
                    <ms:personalDataIncluded>false</ms:personalDataIncluded>
                    <ms:sensitiveDataIncluded>false</ms:sensitiveDataIncluded>
				</ms:Corpus>
			</ms:LRSubclass>
			</ms:LanguageResource>
			</ms:DescribedEntity>
		</ms:MetadataRecord>

.. _BulTM:

Bilingual corpus
==================

.. raw:: html

    <button class="btn btn-example-data" onclick="$('#bilingualCorpus').toggle(300)">Greek-Bulgarian Bul-TM parallel corpus</button>
    <div id="bilingualCorpus" style="display: none;">
	
.. code-block:: xml
   
    <?xml version="1.0" encoding="utf-8"?>
    <ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://inventory.clarin.gr/metadata-schema/CLARIN-SHARE.xsd">
    <ms:metadataCreationDate>2015-09-11</ms:metadataCreationDate>
    <ms:metadataLastDateUpdated>2021-05-28</ms:metadataLastDateUpdated>
    <ms:metadataCurator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCurator>
    <ms:compliesWith>http://w3id.org/meta-share/meta-share/CLARIN-SHARE</ms:compliesWith>
    <ms:metadataCreator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCreator>
    <ms:sourceOfMetadataRecord>
        <ms:repositoryName xml:lang="el">Αποθετήριο ΕΚ ΑΘΗΝΑ</ms:repositoryName>
        <ms:repositoryName xml:lang="en">ATHENA RC Repository</ms:repositoryName>
        <ms:repositoryURL>http://inventory.clarin.gr</ms:repositoryURL>
    </ms:sourceOfMetadataRecord>
    <ms:DescribedEntity>
        <ms:LanguageResource>
            <ms:entityType>LanguageResource</ms:entityType>
            <ms:resourceName xml:lang="el">Ελληνο-Βουλγαρικό κειμενικό σώμα Bul-TM</ms:resourceName>
            <ms:resourceName xml:lang="en">Greek-Bulgarian Bul-TM parallel corpus</ms:resourceName>
            <ms:resourceShortName xml:lang="en">Bul-TM</ms:resourceShortName>
            <ms:description xml:lang="el">Δίγλωσσο σώμα παράλληλων προτάσεων (βουλγαρικά – ελληνικά)
                από κείμενα γενικής γλώσσας που έχουν αντληθεί από το διαδίκτυο. Το σώμα διατίθεται
                με τη μορφή TMX (ως στοιχισμένες προτάσεις).</ms:description>
            <ms:description xml:lang="en">Parallel bilingual corpus (web documents, general domain)
                aligned at sentence level; the corpus is available in TMX format.</ms:description>
            <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                >http://hdl.handle.net/11500/ATHENA-0000-0000-23E4-7</ms:LRIdentifier>
            <ms:version>1.0.0 (automatically assigned)</ms:version>
            <ms:additionalInfo>
                <ms:email>person@ilsp.gr</ms:email>
            </ms:additionalInfo>
            <ms:additionalInfo>
                <ms:email>person@ilsp.gr</ms:email>
            </ms:additionalInfo>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:citationText xml:lang="el">Ινστιτούτο Επεξεργασίας του Λόγου - Ερευνητικό Κέντρο
                Αθηνά (2015). Ελληνο-Βουλγαρικό κειμενικό σώμα Bul-TM. Version 1.0.0 (automatically
                assigned). [Dataset (Text corpus)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-23E4-7</ms:citationText>
            <ms:citationText xml:lang="en">Institute for Language and Speech Processing - Athena
                Research Center (2015). Greek-Bulgarian Bul-TM parallel corpus. Version 1.0.0
                (automatically assigned). [Dataset (Text corpus)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-23E4-7</ms:citationText>
            <ms:keyword xml:lang="en">bilingual</ms:keyword>
            <ms:keyword xml:lang="en">parallel</ms:keyword>
            <ms:keyword xml:lang="en">alignment</ms:keyword>
            <ms:keyword xml:lang="en">writtenLanguage</ms:keyword>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">Political Science</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC320</ms:DomainIdentifier>
            </ms:domain>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">society</ms:categoryLabel>
            </ms:domain>
            <ms:resourceCreator>
                <ms:Organization>
                    <ms:actorType>Organization</ms:actorType>
                    <ms:organizationName xml:lang="el">Ινστιτούτο Επεξεργασίας του
                        Λόγου</ms:organizationName>
                    <ms:organizationName xml:lang="en">Institute for Language and Speech
                        Processing</ms:organizationName>
                    <ms:website>http://www.ilsp.gr/</ms:website>
                </ms:Organization>
            </ms:resourceCreator>
            <ms:creationStartDate>2005-10-01</ms:creationStartDate>
            <ms:creationEndDate>2007-09-30</ms:creationEndDate>
            <ms:fundingProject>
                <ms:projectName xml:lang="en">Development of a Bulgarian to Greek and Greek to
                    Bulgarian Translation Memory workbench</ms:projectName>
                <ms:website>https://www.ilsp.gr/projects/bultm/</ms:website>
                <ms:fundingType>http://w3id.org/meta-share/meta-share/nationalFunds</ms:fundingType>
                <ms:funder>
                    <ms:Organization>
                        <ms:actorType>Organization</ms:actorType>
                        <ms:organizationName xml:lang="en">Ministry of Economy and
                            Finances</ms:organizationName>
                    </ms:Organization>
                </ms:funder>
            </ms:fundingProject>
            <ms:intendedApplication>
                <ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LanguageTechnology</ms:LTClassRecommended>
            </ms:intendedApplication>
            <ms:intendedApplication>
                <ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/MachineTranslation</ms:LTClassRecommended>
            </ms:intendedApplication>
            <ms:actualUse>
                <ms:usedInApplication>
                    <ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/MachineTranslation</ms:LTClassRecommended>
                </ms:usedInApplication>
                <ms:usedInApplication>
                    <ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LanguageTechnology</ms:LTClassRecommended>
                </ms:usedInApplication>
            </ms:actualUse>
            <ms:isDocumentedBy>
                <ms:title xml:lang="el">Π2.1-Σώματα_Κειμένων-v1</ms:title>
                <ms:title xml:lang="en">Deliverable 2.1 - Text corpora-v1</ms:title>
            </ms:isDocumentedBy>
            <ms:LRSubclass>
                <ms:Corpus>
                    <ms:lrType>Corpus</ms:lrType>
                    <ms:corpusSubclass>http://w3id.org/meta-share/meta-share/annotatedCorpus</ms:corpusSubclass>
                    <ms:CorpusMediaPart>
                        <ms:CorpusTextPart>
                            <ms:corpusMediaType>CorpusTextPart</ms:corpusMediaType>
                            <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
                            <ms:lingualityType>http://w3id.org/meta-share/meta-share/bilingual</ms:lingualityType>
                            <ms:multilingualityType>http://w3id.org/meta-share/meta-share/parallel</ms:multilingualityType>
                            <ms:language>
                                <ms:languageTag>el</ms:languageTag>
                                <ms:languageId>el</ms:languageId>
                            </ms:language>
                            <ms:language>
                                <ms:languageTag>bg</ms:languageTag>
                                <ms:languageId>bg</ms:languageId>
                            </ms:language>
                            <ms:modalityType>http://w3id.org/meta-share/meta-share/writtenLanguage</ms:modalityType>
                            <ms:annotation>
                                <ms:annotationType>http://w3id.org/meta-share/omtd-share/Alignment1</ms:annotationType>
                                <ms:segmentationLevel>http://w3id.org/meta-share/meta-share/sentence</ms:segmentationLevel>
                                <ms:annotationStandoff>false</ms:annotationStandoff>
                                <ms:annotationMode>http://w3id.org/meta-share/meta-share/automatic</ms:annotationMode>
                                <ms:isAnnotatedBy>
                                    <ms:resourceName xml:lang="en">TrAid</ms:resourceName>
                                    <ms:version>unspecified</ms:version>
                                </ms:isAnnotatedBy>
                            </ms:annotation>
                            <ms:hasOriginalSource>
                                <ms:resourceName xml:lang="en">The JRC-Acquis Corpus, version
                                    3.0</ms:resourceName>
                                <ms:LRIdentifier
                                    ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                                    >http://hdl.handle.net/11500/ATHENA-0000-0000-25C9-4</ms:LRIdentifier>
                                <ms:version>1.0.0 (automatically assigned)</ms:version>
                            </ms:hasOriginalSource>
                            <ms:hasOriginalSource>
                                <ms:resourceName xml:lang="en">SETIMES - A parallel corpus of the
                                    Balkan languages</ms:resourceName>
                                <ms:LRIdentifier
                                    ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                                    >http://hdl.handle.net/11500/ATHENA-0000-0000-2591-2</ms:LRIdentifier>
                                <ms:version>1</ms:version>
                            </ms:hasOriginalSource>
                            <ms:creationDetails xml:lang="en">original source: EU
                                texts</ms:creationDetails>
                        </ms:CorpusTextPart>
                    </ms:CorpusMediaPart>
                    <ms:DatasetDistribution>
                        <ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
                        <ms:downloadLocation>http://www.hiddenLocation.org</ms:downloadLocation>
                        <ms:accessLocation>http://fixme.com</ms:accessLocation>
                        <ms:distributionTextFeature>
                            <ms:size>
                                <ms:amount>10000000.0</ms:amount>
                                <ms:sizeUnit>http://w3id.org/meta-share/meta-share/token</ms:sizeUnit>
                            </ms:size>
                            <ms:dataFormat>http://w3id.org/meta-share/omtd-share/Tmx</ms:dataFormat>
                            <ms:characterEncoding>http://w3id.org/meta-share/meta-share/UTF-8</ms:characterEncoding>
                        </ms:distributionTextFeature>
                        <ms:licenceTerms>
                            <ms:licenceTermsName xml:lang="en">Creative Commons Attribution 4.0
                                International</ms:licenceTermsName>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by/4.0/legalcode</ms:licenceTermsURL>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by/4.0/</ms:licenceTermsURL>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/attribution</ms:conditionOfUse>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsDirectAccess</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsProcessing</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/public</ms:licenceCategory>
                            <ms:LicenceIdentifier
                                ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX"
                                >CC-BY-4.0</ms:LicenceIdentifier>
                        </ms:licenceTerms>
                        <ms:attributionText xml:lang="el">Ελληνο-Βουλγαρικό κειμενικό σώμα Bul-TM.
                            Δημιουργός: Ινστιτούτο Επεξεργασίας του Λόγου - Ερευνητικό Κέντρο Αθηνά.
                            Άδεια: Creative Commons Attribution 4.0 International
                            (https://creativecommons.org/licenses/by/4.0/legalcode,
                            https://creativecommons.org/licenses/by/4.0/). Πηγή:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-23E4-7
                            (CLARIN:EL)</ms:attributionText>
                        <ms:attributionText xml:lang="en">Greek-Bulgarian Bul-TM parallel corpus by
                            Institute for Language and Speech Processing - Athena Research Center
                            used under Creative Commons Attribution 4.0 International
                            (https://creativecommons.org/licenses/by/4.0/legalcode,
                            https://creativecommons.org/licenses/by/4.0/). Source:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-23E4-7
                            (CLARIN:EL)</ms:attributionText>
                    </ms:DatasetDistribution>
                    <ms:personalDataIncluded>false</ms:personalDataIncluded>
                    <ms:sensitiveDataIncluded>false</ms:sensitiveDataIncluded>
                </ms:Corpus>
            </ms:LRSubclass>
        </ms:LanguageResource>
    </ms:DescribedEntity>
    </ms:MetadataRecord>

.. _DictaSign:

Multilingual corpus
======================

.. raw:: html

    <button class="btn btn-example-data" onclick="$('#multilingualCorpus').toggle(300)">DICTA-SIGN corpus</button>
    <div id="multilingualCorpus" style="display: none;">
	
.. code-block:: xml

	<?xml version="1.0" encoding="utf-8"?>
	<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://inventory.clarin.gr/metadata-schema/CLARIN-SHARE.xsd">
    <ms:metadataCreationDate>2015-12-23</ms:metadataCreationDate>
    <ms:metadataLastDateUpdated>2021-05-28</ms:metadataLastDateUpdated>
    <ms:metadataCurator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCurator>
    <ms:compliesWith>http://w3id.org/meta-share/meta-share/CLARIN-SHARE</ms:compliesWith>
    <ms:metadataCreator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCreator>
    <ms:sourceOfMetadataRecord>
        <ms:repositoryName xml:lang="el">Αποθετήριο ΕΚ ΑΘΗΝΑ</ms:repositoryName>
        <ms:repositoryName xml:lang="en">ATHENA RC Repository</ms:repositoryName>
        <ms:repositoryURL>http://inventory.clarin.gr</ms:repositoryURL>
    </ms:sourceOfMetadataRecord>
    <ms:DescribedEntity>
        <ms:LanguageResource>
            <ms:entityType>LanguageResource</ms:entityType>
            <ms:resourceName xml:lang="el">Σώμα DICTA-SIGN</ms:resourceName>
            <ms:resourceName xml:lang="en">DICTA-SIGN corpus</ms:resourceName>
            <ms:description xml:lang="el">Σώμα πολυμεσικών δεδομένων (βίντεο) για τέσσερις
                νοηματικές γλώσσες (ελληνική, αγγλική, γαλλική και γερμανική). Περιλαμβάνονται
                καταγραφές από τουλάχιστον 14 πληροφορητές για κάθε γλώσσα σε τουλάχιστον δίωρες
                συνεδρίες, με βάση κοινά σενάρια και καθήκοντα για όλες τις γλώσσες. Τα δεδομένα
                είναι εν μέρει επισημειωμένα. Το σώμα δεδομένων διατίθεται σε ιστότοπο που έχει
                δημιουργηθεί και συντηρείται από το Πανεπιστήμιο του Αμβούργου και είναι προσβάσιμος
                &lt;a href="http://www.sign-lang.uni-hamburg.de/dicta-sign/portal/index.html"
                target="_blank"&gt;εδώ&lt;/a&gt;.</ms:description>
            <ms:description xml:lang="en">Multimedia corpus (video) for four sign languages
                (english, french, german and greek) of at least 14 informants per language and a
                session duration of approx. 2 hours using the same elicitation materials (scripts
                and tasks) across languages. The data is partially annotated. The corpus is
                available through a dedicated website, created and maintained by the University of
                Hamburg accessible &lt;a
                href="http://www.sign-lang.uni-hamburg.de/dicta-sign/portal/index.html"
                target="_blank"&gt;here&lt;/a&gt;.</ms:description>
            <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                >http://hdl.handle.net/11500/ATHENA-0000-0000-28C5-5</ms:LRIdentifier>
            <ms:version>1.0.0 (automatically assigned)</ms:version>
            <ms:additionalInfo>
                <ms:landingPage>http://www.sign-lang.uni-hamburg.de/dicta-sign/portal/index.html</ms:landingPage>
            </ms:additionalInfo>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:citationText xml:lang="el">School of Computing Sciences - University of East Anglia;
                Research Institute of Computer Science - Paul Sabatier University; Institute of
                German Sign Language and Communication of the Deaf - University of Hamburg (2015).
                Σώμα DICTA-SIGN. Version 1.0.0 (automatically assigned). [Dataset (Text and Video
                corpus)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-28C5-5</ms:citationText>
            <ms:citationText xml:lang="en">School of Computing Sciences - University of East Anglia;
                Research Institute of Computer Science - Paul Sabatier University; Institute of
                German Sign Language and Communication of the Deaf - University of Hamburg (2015).
                DICTA-SIGN corpus. Version 1.0.0 (automatically assigned). [Dataset (Text and Video
                corpus)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-28C5-5</ms:citationText>
            <ms:keyword xml:lang="en">multilingual</ms:keyword>
            <ms:keyword xml:lang="en">parallel</ms:keyword>
            <ms:keyword xml:lang="en">scripts</ms:keyword>
            <ms:keyword xml:lang="en">signLanguage</ms:keyword>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">Geography &amp; Travel</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC910</ms:DomainIdentifier>
            </ms:domain>
            <ms:resourceCreator>
                <ms:Organization>
                    <ms:actorType>Organization</ms:actorType>
                    <ms:organizationName xml:lang="en">School of Computing
                        Sciences</ms:organizationName>
                    <ms:website>https://www.uea.ac.uk/about/school-of-computing-sciences</ms:website>
                </ms:Organization>
            </ms:resourceCreator>
            <ms:resourceCreator>
                <ms:Organization>
                    <ms:actorType>Organization</ms:actorType>
                    <ms:organizationName xml:lang="en">Research Institute of Computer
                        Science</ms:organizationName>
                    <ms:organizationName xml:lang="fr">Institut de Recherche en Informatique de
                        Toulouse</ms:organizationName>
                    <ms:website>http://www.irit.fr/</ms:website>
                </ms:Organization>
            </ms:resourceCreator>
            <ms:resourceCreator>
                <ms:Organization>
                    <ms:actorType>Organization</ms:actorType>
                    <ms:organizationName xml:lang="en">Institute of German Sign Language and
                        Communication of the Deaf</ms:organizationName>
                    <ms:website>https://www.idgs.uni-hamburg.de/en.html</ms:website>
                </ms:Organization>
            </ms:resourceCreator>
            <ms:creationStartDate>2009-02-01</ms:creationStartDate>
            <ms:creationEndDate>2012-01-31</ms:creationEndDate>
            <ms:fundingProject>
                <ms:projectName xml:lang="en">Sign Language Recognition, Generation and Modelling
                    with application in Deaf Communication</ms:projectName>
                <ms:website>http://www.dictasign.eu/</ms:website>
                <ms:fundingType>http://w3id.org/meta-share/meta-share/euFunds</ms:fundingType>
                <ms:funder>
                    <ms:Organization>
                        <ms:actorType>Organization</ms:actorType>
                        <ms:organizationName xml:lang="el">Ευρωπαϊκή Επιτροπή</ms:organizationName>
                        <ms:organizationName xml:lang="en">European Commission</ms:organizationName>
                        <ms:website>https://ec.europa.eu/info/index_en</ms:website>
                    </ms:Organization>
                </ms:funder>
            </ms:fundingProject>
            <ms:intendedApplication>
                <ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LanguageTechnology</ms:LTClassRecommended>
            </ms:intendedApplication>
            <ms:actualUse>
                <ms:usedInApplication>
                    <ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LanguageTechnology</ms:LTClassRecommended>
                </ms:usedInApplication>
            </ms:actualUse>
            <ms:LRSubclass>
                <ms:Corpus>
                    <ms:lrType>Corpus</ms:lrType>
                    <ms:corpusSubclass>http://w3id.org/meta-share/meta-share/rawCorpus</ms:corpusSubclass>
                    <ms:CorpusMediaPart>
                        <ms:CorpusTextPart>
                            <ms:corpusMediaType>CorpusTextPart</ms:corpusMediaType>
                            <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
                            <ms:lingualityType>http://w3id.org/meta-share/meta-share/multilingual</ms:lingualityType>
                            <ms:multilingualityType>http://w3id.org/meta-share/meta-share/unspecified</ms:multilingualityType>
                            <ms:language>
                                <ms:languageTag>fr</ms:languageTag>
                                <ms:languageId>fr</ms:languageId>
                            </ms:language>
                            <ms:language>
                                <ms:languageTag>el</ms:languageTag>
                                <ms:languageId>el</ms:languageId>
                            </ms:language>
                            <ms:language>
                                <ms:languageTag>de</ms:languageTag>
                                <ms:languageId>de</ms:languageId>
                            </ms:language>
                            <ms:language>
                                <ms:languageTag>en</ms:languageTag>
                                <ms:languageId>en</ms:languageId>
                            </ms:language>
                            <ms:modalityType>http://w3id.org/meta-share/meta-share/signLanguage</ms:modalityType>
                            <ms:TextGenre>
                                <ms:categoryLabel xml:lang="en">scripts</ms:categoryLabel>
                            </ms:TextGenre>
                            <ms:linkToOtherMedia>
                                <ms:otherMedia>http://w3id.org/meta-share/meta-share/video</ms:otherMedia>
                                <ms:mediaTypeDetails xml:lang="en">scripts of the tasks for the
                                    video</ms:mediaTypeDetails>
                                <ms:synchronizedWithVideo>false</ms:synchronizedWithVideo>
                            </ms:linkToOtherMedia>
                        </ms:CorpusTextPart>
                    </ms:CorpusMediaPart>
                    <ms:CorpusMediaPart>
                        <ms:CorpusVideoPart>
                            <ms:corpusMediaType>CorpusVideoPart</ms:corpusMediaType>
                            <ms:mediaType>http://w3id.org/meta-share/meta-share/video</ms:mediaType>
                            <ms:lingualityType>http://w3id.org/meta-share/meta-share/multilingual</ms:lingualityType>
                            <ms:multilingualityType>http://w3id.org/meta-share/meta-share/parallel</ms:multilingualityType>
                            <ms:language>
                                <ms:languageTag>gss</ms:languageTag>
                                <ms:languageId>gss</ms:languageId>
                            </ms:language>
                            <ms:language>
                                <ms:languageTag>bfi</ms:languageTag>
                                <ms:languageId>bfi</ms:languageId>
                            </ms:language>
                            <ms:language>
                                <ms:languageTag>gsg</ms:languageTag>
                                <ms:languageId>gsg</ms:languageId>
                            </ms:language>
                            <ms:language>
                                <ms:languageTag>fsl</ms:languageTag>
                                <ms:languageId>fsl</ms:languageId>
                            </ms:language>
                            <ms:modalityType>http://w3id.org/meta-share/meta-share/signLanguage</ms:modalityType>
                            <ms:typeOfVideoContent xml:lang="en">natural
                                signers</ms:typeOfVideoContent>
                            <ms:textIncludedInVideo>http://w3id.org/meta-share/meta-share/none1</ms:textIncludedInVideo>
                            <ms:naturality>http://w3id.org/meta-share/meta-share/elicited</ms:naturality>
                            <ms:conversationalType>http://w3id.org/meta-share/meta-share/dialogue</ms:conversationalType>
                            <ms:scenarioType>http://w3id.org/meta-share/meta-share/rolePlay</ms:scenarioType>
                            <ms:audience>http://w3id.org/meta-share/meta-share/none3</ms:audience>
                            <ms:interactivity>http://w3id.org/meta-share/meta-share/interactive1</ms:interactivity>
                            <ms:linkToOtherMedia>
                                <ms:otherMedia>http://w3id.org/meta-share/meta-share/text</ms:otherMedia>
                                <ms:mediaTypeDetails xml:lang="en">scripts of the tasks for the
                                    video</ms:mediaTypeDetails>
                                <ms:synchronizedWithText>false</ms:synchronizedWithText>
                            </ms:linkToOtherMedia>
                        </ms:CorpusVideoPart>
                    </ms:CorpusMediaPart>
                    <ms:DatasetDistribution>
                        <ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/accessibleThroughInterface</ms:DatasetDistributionForm>
                        <ms:accessLocation>http://www.sign-lang.uni-hamburg.de/dicta-sign/portal/index.html</ms:accessLocation>
                        <ms:distributionTextFeature>
                            <ms:size>
                                <ms:amount>10.0</ms:amount>
                                <ms:sizeUnit>http://w3id.org/meta-share/meta-share/file</ms:sizeUnit>
                            </ms:size>
                            <ms:dataFormat>http://w3id.org/meta-share/meta-share/unspecified</ms:dataFormat>
                        </ms:distributionTextFeature>
                        <ms:distributionVideoFeature>
                            <ms:size>
                                <ms:amount>25.0</ms:amount>
                                <ms:sizeUnit>http://w3id.org/meta-share/meta-share/hour1</ms:sizeUnit>
                            </ms:size>
                            <ms:dataFormat>http://w3id.org/meta-share/omtd-share/mp4</ms:dataFormat>
                        </ms:distributionVideoFeature>
                        <ms:licenceTerms>
                            <ms:licenceTermsName xml:lang="en">Creative Commons Attribution Non
                                Commercial No Derivatives 4.0 International</ms:licenceTermsName>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode</ms:licenceTermsURL>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by-nc-nd/4.0/</ms:licenceTermsURL>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/attribution</ms:conditionOfUse>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/nonCommercialUse</ms:conditionOfUse>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/noDerivatives</ms:conditionOfUse>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsDirectAccess</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/public</ms:licenceCategory>
                            <ms:LicenceIdentifier
                                ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX"
                                >CC-BY-NC-ND-4.0</ms:LicenceIdentifier>
                        </ms:licenceTerms>
                        <ms:attributionText xml:lang="el">Σώμα DICTA-SIGN. Δημιουργός: School of
                            Computing Sciences - University of East Anglia, Research Institute of
                            Computer Science - Paul Sabatier University and Institute of German Sign
                            Language and Communication of the Deaf - University of Hamburg. Άδεια:
                            Creative Commons Attribution Non Commercial No Derivatives 4.0
                            International
                            (https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode,
                            https://creativecommons.org/licenses/by-nc-nd/4.0/). Πηγή:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-28C5-5
                            (CLARIN:EL)</ms:attributionText>
                        <ms:attributionText xml:lang="en">DICTA-SIGN corpus by School of Computing
                            Sciences - University of East Anglia, Research Institute of Computer
                            Science - Paul Sabatier University and Institute of German Sign Language
                            and Communication of the Deaf - University of Hamburg used under
                            Creative Commons Attribution Non Commercial No Derivatives 4.0
                            International
                            (https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode,
                            https://creativecommons.org/licenses/by-nc-nd/4.0/). Source:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-28C5-5
                            (CLARIN:EL)</ms:attributionText>
                    </ms:DatasetDistribution>
                    <ms:personalDataIncluded>false</ms:personalDataIncluded>
                    <ms:sensitiveDataIncluded>false</ms:sensitiveDataIncluded>
                </ms:Corpus>
            </ms:LRSubclass>
        </ms:LanguageResource>
	</ms:DescribedEntity>
	</ms:MetadataRecord>

.. _LCRXML:

2. Lexical/Conceptual resources (LCR)
***************************************

.. _Kelly:

Monolingual LCR
========================

.. raw:: html

    <button class="btn btn-example-data" onclick="$('#monolingualLexical').toggle(300)">KELLY word-list EL</button>
    <div id="monolingualLexical" style="display: none;">
	
.. code-block:: xml

    <?xml version="1.0" encoding="utf-8"?>
    <ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://inventory.clarin.gr/metadata-schema/CLARIN-SHARE.xsd">
    <ms:metadataCreationDate>2015-12-08</ms:metadataCreationDate>
    <ms:metadataLastDateUpdated>2021-05-28</ms:metadataLastDateUpdated>
    <ms:metadataCurator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCurator>
    <ms:compliesWith>http://w3id.org/meta-share/meta-share/CLARIN-SHARE</ms:compliesWith>
    <ms:metadataCreator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCreator>
    <ms:sourceOfMetadataRecord>
        <ms:repositoryName xml:lang="el">Αποθετήριο ΕΚ ΑΘΗΝΑ</ms:repositoryName>
        <ms:repositoryName xml:lang="en">ATHENA RC Repository</ms:repositoryName>
        <ms:repositoryURL>http://inventory.clarin.gr</ms:repositoryURL>
    </ms:sourceOfMetadataRecord>
    <ms:DescribedEntity>
        <ms:LanguageResource>
            <ms:entityType>LanguageResource</ms:entityType>
            <ms:resourceName xml:lang="en">KELLY word-list Greek</ms:resourceName>
            <ms:resourceShortName xml:lang="en">KELLY word-list EL</ms:resourceShortName>
            <ms:description xml:lang="el">Ο μονόγλωσσος πόρος KELLY EL αποτελεί μέρος του ψηφιακού
                εκπαιδευτικού υλικού που αναπτύχθηκε για εννέα γλώσσες, από τις οποίες 4 ευρέως
                διδασκόμενες (Αγγλικά, Αραβικά, Ρωσικά και Κινέζικα) και 5 λιγότερο διδασκόμενες
                (Ελληνικά, Ιταλικά, Σουηδικά, Πολωνικά και Νορβηγικά) με σκοπό να υποβοηθήσει την
                εκμάθησή τους ως ξένης/δεύτερης γλώσσας. Πιο συγκεκριμένα, πρόκειται για το ελληνικό
                κομμάτι ενός υλικού το οποίο ολοκληρώνεται από μία σειρά μονόγλωσσων και δίγλωσσων
                εγγραφών καλύπτοντας συνολικά 36 γλωσσικά ζεύγη. Η επιλογή των λέξεων βασίστηκε σε
                ψηφιακούς γλωσσικούς πόρους για κάθε γλώσσα, η επιλογή των οποίων ακολούθησε κοινές
                προδιαγραφές σε όλες τις γλώσσες, με στόχο την εξασφάλιση ομοιόμορφου γλωσσικού
                υλικού. Συνδυάστηκε δε με επεξεργασία των αποτελεσμάτων από γλωσσολόγους και
                εκπαιδευτικούς για την ένταξη κάθε λέξης στο κατάλληλο επίπεδο του Κοινού Ευρωπαϊκού
                Πλαισίου Αναφοράς (CEFR, &lt;a
                href="https://www.coe.int/en/web/common-european-framework-reference-languages"
                target="_blank"&gt;Common European Framework of Reference for Languages&lt;/a&gt;).
                Η επιλογή του λεξιλογίου υιοθέτησε διαδικασία εξαγωγής γνώσης από τα κείμενα
                (εφαρμογή ακολουθιών τεχνολογιών όπως δομική επισημείωση κειμένου, μορφολογική
                επισημείωση και λημματοποίηση), ακολουθούμενη από εφαρμογή στατιστικών μετρικών για
                την εξαγωγή των πιο συχνών (και άρα απαραίτητων στην εκμάθηση της γλώσσας) λέξεων. Η
                παιδαγωγική διάσταση καθόρισε τις αρχές επεξεργασίας των αποτελεσμάτων της γλωσσικής
                τεχνολογίας από ειδικούς, οι οποίοι αξιολόγησαν τα αποτελέσματα της
                αυτοματοποιημένης διαδικασίας εξαγωγής των λέξεων από τα κείμενα, κατέταξαν τις
                λέξεις στα επίπεδα του CEFR, και αντιστοίχισαν διαγλωσσικά τις λέξεις, καταρτίζοντας
                λίστες λεξιλογίου για 36 ζεύγη γλωσσών. Στην &lt;a
                href="https://spraakbanken.gu.se/eng/kelly" target="_blank"&gt;επίσημη
                ιστοσελίδα&lt;/a&gt; υπάρχουν περισσότερες πληροφορίες για το έργο Kelly. Από το
                ίδιο σημείο είναι διαθέσιμες οι λίστες λέξεων για τα Αγγλικά, Αραβικά, Ιταλικά,
                Κινέζικα, Νορβηγικά και Ρωσικά, ενώ υπάρχει και &lt;a
                href="http://kelly.sketchengine.co.uk/" target="_blank"&gt;διεπαφή&lt;/a&gt; που
                προσφέρει πρόσβαση στη βάση δεδομένων, όπου μπορεί κανείς να αναζητήσει μια λέξη και
                να δει τις μεταφράσεις της στις άλλες γλώσσες.</ms:description>
            <ms:description xml:lang="en">The monolingual lexical conceptual resource KELLY EL is
                part of digital material created for educational purposes, i.e. to facilitate the
                learning of a foreign/second language. Nine different languages were involved, four
                commonly learned (English, Arabic, Russian and Chinese) and five less commonly
                learned (Greek, Italian, Swedish, Polish and Norwegian). More precisely, KELLY EL is
                the Greek part of a material which consists of monolingual and bilingual word-lists
                covering 36 language pairs in total. The choice of words was based for each language
                on digital language resources. The same standards were applied to all languages for
                the choice of these digital language resources in order to ensure uniformity. The
                material was analyzed and edited by linguists and education professionals and each
                word was mapped to the appropriate language level of the Common European Framework
                of Reference (CEFR, &lt;a
                href="https://www.coe.int/en/web/common-european-framework-reference-languages"
                target="_blank"&gt;Common European Framework of Reference for Languages&lt;/a&gt;).
                The vocabularies produced were created by extracting knowledge from texts (processes
                such as lemmatization, morphological and structural annotation were used) followed
                by statistical metrics techniques for extracting the most frequent (and therefore
                necessary for language learning) words. The language technology results were
                examined by experts according to pedagogical principles, evaluated and finally
                bilingual word-lists with words mapped to the different levels of CEFR for 36
                language pairs were created. More information about Kelly is available from the
                &lt;a href="https://spraakbanken.gu.se/eng/kelly" target="_blank"&gt;official
                project site&lt;/a&gt;, where the lists in English, Arabic, Italian, Chinese,
                Norwegian and Russian can be downloaded. There is also a &lt;a
                href="http://kelly.sketchengine.co.uk/" target="_blank"&gt;database interface
                &lt;/a&gt;, which can be used to explore the links between words selected for each
                of these languages.</ms:description>
            <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                >http://hdl.handle.net/11500/ATHENA-0000-0000-25C1-C</ms:LRIdentifier>
            <ms:version>1.0.0 (automatically assigned)</ms:version>
            <ms:additionalInfo>
                <ms:email>person@ilsp.athena-innovation.gr</ms:email>
            </ms:additionalInfo>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:citationText xml:lang="el">Ινστιτούτο Επεξεργασίας του Λόγου - Ερευνητικό Κέντρο
                Αθηνά (2015). KELLY word-list Greek. Version 1.0.0 (automatically assigned).
                [Dataset (Lexical/Conceptual Resource)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-25C1-C</ms:citationText>
            <ms:citationText xml:lang="en">Institute for Language and Speech Processing - Athena
                Research Center (2015). KELLY word-list Greek. Version 1.0.0 (automatically
                assigned). [Dataset (Lexical/Conceptual Resource)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-25C1-C</ms:citationText>
            <ms:keyword xml:lang="en">monolingual</ms:keyword>
            <ms:resourceCreator>
                <ms:Organization>
                    <ms:actorType>Organization</ms:actorType>
                    <ms:organizationName xml:lang="el">Ινστιτούτο Επεξεργασίας του
                        Λόγου</ms:organizationName>
                    <ms:organizationName xml:lang="en">Institute for Language and Speech
                        Processing</ms:organizationName>
                    <ms:website>http://www.ilsp.gr/</ms:website>
                </ms:Organization>
            </ms:resourceCreator>
            <ms:isToBeCitedBy>
                <ms:title xml:lang="en">Kilgarriff, Adam, Frieda Charalabopoulou, Maria Gavrilidou,
                    Janne Bondi Johannessen, Saussan Khalil, Sofie Johansson Kokkinakis, Robert Lew,
                    Serge Sharoff, Ravikiran Vadlapudi and Elena Volodina (2014) Corpus-based
                    vocabulary lists for language learners for nine languages. Language Resources
                    and Evaluation, 48:121–163</ms:title>
                <ms:DocumentIdentifier
                    ms:DocumentIdentifierScheme="http://purl.org/spar/datacite/doi"
                    >https://doi.org/10.1007/s10579-013-9251-2</ms:DocumentIdentifier>
            </ms:isToBeCitedBy>
            <ms:isPartOf>
                <ms:resourceName xml:lang="en">KELLY word-lists</ms:resourceName>
                <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                    >http://hdl.handle.net/11500/ATHENA-0000-0000-5862-F</ms:LRIdentifier>
                <ms:version>1.0.0 (automatically assigned)</ms:version>
            </ms:isPartOf>
            <ms:LRSubclass>
                <ms:LexicalConceptualResource>
                    <ms:lrType>LexicalConceptualResource</ms:lrType>
                    <ms:lcrSubclass>http://w3id.org/meta-share/meta-share/wordlist</ms:lcrSubclass>
                    <ms:encodingLevel>http://w3id.org/meta-share/meta-share/other</ms:encodingLevel>
                    <ms:LexicalConceptualResourceMediaPart>
                        <ms:LexicalConceptualResourceTextPart>
                            <ms:lcrMediaType>LexicalConceptualResourceTextPart</ms:lcrMediaType>
                            <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
                            <ms:lingualityType>http://w3id.org/meta-share/meta-share/monolingual</ms:lingualityType>
                            <ms:language>
                                <ms:languageTag>el</ms:languageTag>
                                <ms:languageId>el</ms:languageId>
                            </ms:language>
                        </ms:LexicalConceptualResourceTextPart>
                    </ms:LexicalConceptualResourceMediaPart>
                    <ms:DatasetDistribution>
                        <ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
                        <ms:downloadLocation>http://www.hiddenLocation.org</ms:downloadLocation>
                        <ms:distributionTextFeature>
                            <ms:size>
                                <ms:amount>7385.0</ms:amount>
                                <ms:sizeUnit>http://w3id.org/meta-share/meta-share/entry</ms:sizeUnit>
                            </ms:size>
                            <ms:dataFormat>http://w3id.org/meta-share/meta-share/unspecified</ms:dataFormat>
                        </ms:distributionTextFeature>
                        <ms:licenceTerms>
                            <ms:licenceTermsName xml:lang="en">Creative Commons Attribution Non
                                Commercial 4.0 International</ms:licenceTermsName>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by-nc/4.0/legalcode</ms:licenceTermsURL>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by-nc/4.0/</ms:licenceTermsURL>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/attribution</ms:conditionOfUse>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/nonCommercialUse</ms:conditionOfUse>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsDirectAccess</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsProcessing</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/public</ms:licenceCategory>
                            <ms:LicenceIdentifier
                                ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX"
                                >CC-BY-NC-4.0</ms:LicenceIdentifier>
                        </ms:licenceTerms>
                        <ms:attributionText xml:lang="el">KELLY word-list Greek. Δημιουργός:
                            Ινστιτούτο Επεξεργασίας του Λόγου - Ερευνητικό Κέντρο Αθηνά. Άδεια:
                            Creative Commons Attribution Non Commercial 4.0 International
                            (https://creativecommons.org/licenses/by-nc/4.0/legalcode,
                            https://creativecommons.org/licenses/by-nc/4.0/). Πηγή:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-25C1-C
                            (CLARIN:EL)</ms:attributionText>
                        <ms:attributionText xml:lang="en">KELLY word-list Greek by Institute for
                            Language and Speech Processing - Athena Research Center used under
                            Creative Commons Attribution Non Commercial 4.0 International
                            (https://creativecommons.org/licenses/by-nc/4.0/legalcode,
                            https://creativecommons.org/licenses/by-nc/4.0/). Source:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-25C1-C
                            (CLARIN:EL)</ms:attributionText>
                    </ms:DatasetDistribution>
                    <ms:personalDataIncluded>false</ms:personalDataIncluded>
                    <ms:sensitiveDataIncluded>false</ms:sensitiveDataIncluded>
                </ms:LexicalConceptualResource>
            </ms:LRSubclass>
        </ms:LanguageResource>
    </ms:DescribedEntity>
    </ms:MetadataRecord>


.. _OrossimoHistory:

Bilingual LCR
========================

.. raw:: html

    <button class="btn btn-example-data" onclick="$('#biingualLexical').toggle(300)">Orossimo Terminological Resource - History</button><div id="biingualLexical" style="display: none;">
	
.. code-block:: xml


	<?xml version="1.0" encoding="utf-8"?>
	<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://inventory.clarin.gr/metadata-schema/CLARIN-SHARE.xsd">
    <ms:metadataCreationDate>2016-12-07</ms:metadataCreationDate>
    <ms:metadataLastDateUpdated>2021-05-28</ms:metadataLastDateUpdated>
    <ms:metadataCurator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCurator>
    <ms:compliesWith>http://w3id.org/meta-share/meta-share/CLARIN-SHARE</ms:compliesWith>
    <ms:metadataCreator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCreator>
    <ms:sourceOfMetadataRecord>
        <ms:repositoryName xml:lang="el">Αποθετήριο ΕΚ ΑΘΗΝΑ</ms:repositoryName>
        <ms:repositoryName xml:lang="en">ATHENA RC Repository</ms:repositoryName>
        <ms:repositoryURL>http://inventory.clarin.gr</ms:repositoryURL>
    </ms:sourceOfMetadataRecord>
    <ms:DescribedEntity>
        <ms:LanguageResource>
            <ms:entityType>LanguageResource</ms:entityType>
            <ms:resourceName xml:lang="el">Ορολογικός Πόρος ΟΡΟΣΗΜΟ - Ιστορία</ms:resourceName>
            <ms:resourceName xml:lang="en">Orossimo Terminological Resource -
                History</ms:resourceName>
            <ms:description xml:lang="el">Πρόκειται για δίγλωσσο ορολογικό πόρο που προέρχεται από
                αντίστοιχο σώμα κειμένων ακαδημαϊκού λόγου του γνωστικού τομέα της
                Ιστορίας.</ms:description>
            <ms:description xml:lang="en">A bilingual terminological glossary extracted from
                academic discourse texts belonging to the History domain.</ms:description>
            <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                >http://hdl.handle.net/11500/ATHENA-0000-0000-4B4B-9</ms:LRIdentifier>
            <ms:version>1.0.0 (automatically assigned)</ms:version>
            <ms:additionalInfo>
                <ms:email>person@ilsp</ms:email>
            </ms:additionalInfo>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:citationText xml:lang="el">Ινστιτούτο Επεξεργασίας του Λόγου - Ερευνητικό Κέντρο
                Αθηνά (2016). Ορολογικός Πόρος ΟΡΟΣΗΜΟ - Ιστορία. Version 1.0.0 (automatically
                assigned). [Dataset (Lexical/Conceptual Resource)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-4B4B-9</ms:citationText>
            <ms:citationText xml:lang="en">Institute for Language and Speech Processing - Athena
                Research Center (2016). Orossimo Terminological Resource - History. Version 1.0.0
                (automatically assigned). [Dataset (Lexical/Conceptual Resource)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-4B4B-9</ms:citationText>
            <ms:keyword xml:lang="en">bilingual</ms:keyword>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">History</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC900</ms:DomainIdentifier>
            </ms:domain>
            <ms:resourceCreator>
                <ms:Organization>
                    <ms:actorType>Organization</ms:actorType>
                    <ms:organizationName xml:lang="el">Ινστιτούτο Επεξεργασίας του
                        Λόγου</ms:organizationName>
                    <ms:organizationName xml:lang="en">Institute for Language and Speech
                        Processing</ms:organizationName>
                    <ms:website>http://www.ilsp.gr/</ms:website>
                </ms:Organization>
            </ms:resourceCreator>
            <ms:creationStartDate>1996-01-01</ms:creationStartDate>
            <ms:creationEndDate>1998-12-31</ms:creationEndDate>
            <ms:fundingProject>
                <ms:projectName xml:lang="el">ΟΡΟΣΗΜΟ</ms:projectName>
                <ms:projectName xml:lang="en">OROSSIMO</ms:projectName>
                <ms:website>https://www.ilsp.gr/projects/orosimo/</ms:website>
                <ms:fundingType>http://w3id.org/meta-share/meta-share/nationalFunds</ms:fundingType>
                <ms:funder>
                    <ms:Organization>
                        <ms:actorType>Organization</ms:actorType>
                        <ms:organizationName xml:lang="en">General Secretariat for Research and
                            Technology</ms:organizationName>
                    </ms:Organization>
                </ms:funder>
            </ms:fundingProject>
            <ms:hasOriginalSource>
                <ms:resourceName xml:lang="el">Σώμα κειμένων ΟΡΟΣΗΜΟ - Ιστορία</ms:resourceName>
                <ms:resourceName xml:lang="en">OROSSIMO Corpus - History</ms:resourceName>
                <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                    >http://hdl.handle.net/11500/ATHENA-0000-0000-240F-8</ms:LRIdentifier>
                <ms:version>1.0.0 (automatically assigned)</ms:version>
            </ms:hasOriginalSource>
            <ms:isDocumentedBy>
                <ms:title xml:lang="el">Συλλογή ηλεκτρονικών ορολογικών πόρων: μεθοδολογία και
                    αποτελέσματα</ms:title>
                <ms:title xml:lang="en">Collection of digital terminological resources: methodology
                    and results</ms:title>
            </ms:isDocumentedBy>
            <ms:isPartOf>
                <ms:resourceName xml:lang="el">Ορολογικός Πόρος ΟΡΟΣΗΜΟ</ms:resourceName>
                <ms:resourceName xml:lang="en">Orossimo Terminological Resource</ms:resourceName>
                <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                    >http://hdl.handle.net/11500/ATHENA-0000-0000-4B49-B</ms:LRIdentifier>
                <ms:version>1.0.0 (automatically assigned)</ms:version>
            </ms:isPartOf>
            <ms:LRSubclass>
                <ms:LexicalConceptualResource>
                    <ms:lrType>LexicalConceptualResource</ms:lrType>
                    <ms:lcrSubclass>http://w3id.org/meta-share/meta-share/terminologicalResource</ms:lcrSubclass>
                    <ms:encodingLevel>http://w3id.org/meta-share/meta-share/semantics</ms:encodingLevel>
                    <ms:ContentType>http://w3id.org/meta-share/meta-share/lemma</ms:ContentType>
                    <ms:ContentType>http://w3id.org/meta-share/meta-share/domain1</ms:ContentType>
                    <ms:ContentType>http://w3id.org/meta-share/meta-share/derivation</ms:ContentType>
                    <ms:ContentType>http://w3id.org/meta-share/meta-share/translationEquivalent</ms:ContentType>
                    <ms:ContentType>http://w3id.org/meta-share/meta-share/note1</ms:ContentType>
                    <ms:LexicalConceptualResourceMediaPart>
                        <ms:LexicalConceptualResourceTextPart>
                            <ms:lcrMediaType>LexicalConceptualResourceTextPart</ms:lcrMediaType>
                            <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
                            <ms:lingualityType>http://w3id.org/meta-share/meta-share/bilingual</ms:lingualityType>
                            <ms:language>
                                <ms:languageTag>en</ms:languageTag>
                                <ms:languageId>en</ms:languageId>
                            </ms:language>
                            <ms:language>
                                <ms:languageTag>el</ms:languageTag>
                                <ms:languageId>el</ms:languageId>
                            </ms:language>
                        </ms:LexicalConceptualResourceTextPart>
                    </ms:LexicalConceptualResourceMediaPart>
                    <ms:DatasetDistribution>
                        <ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
                        <ms:downloadLocation>http://www.hiddenLocation.org</ms:downloadLocation>
                        <ms:accessLocation>http://fixme.com</ms:accessLocation>
                        <ms:distributionTextFeature>
                            <ms:size>
                                <ms:amount>2353.0</ms:amount>
                                <ms:sizeUnit>http://w3id.org/meta-share/meta-share/term</ms:sizeUnit>
                            </ms:size>
                            <ms:dataFormat>http://w3id.org/meta-share/omtd-share/MsExcel</ms:dataFormat>
                        </ms:distributionTextFeature>
                        <ms:licenceTerms>
                            <ms:licenceTermsName xml:lang="en">Creative Commons Attribution 4.0
                                International</ms:licenceTermsName>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by/4.0/legalcode</ms:licenceTermsURL>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by/4.0/</ms:licenceTermsURL>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/attribution</ms:conditionOfUse>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsDirectAccess</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsProcessing</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/public</ms:licenceCategory>
                            <ms:LicenceIdentifier
                                ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX"
                                >CC-BY-4.0</ms:LicenceIdentifier>
                        </ms:licenceTerms>
                        <ms:attributionText xml:lang="el">Ορολογικός Πόρος ΟΡΟΣΗΜΟ - Ιστορία.
                            Δημιουργός: Ινστιτούτο Επεξεργασίας του Λόγου - Ερευνητικό Κέντρο Αθηνά.
                            Άδεια: Creative Commons Attribution 4.0 International
                            (https://creativecommons.org/licenses/by/4.0/legalcode,
                            https://creativecommons.org/licenses/by/4.0/). Πηγή:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-4B4B-9
                            (CLARIN:EL)</ms:attributionText>
                        <ms:attributionText xml:lang="en">Orossimo Terminological Resource - History
                            by Institute for Language and Speech Processing - Athena Research Center
                            used under Creative Commons Attribution 4.0 International
                            (https://creativecommons.org/licenses/by/4.0/legalcode,
                            https://creativecommons.org/licenses/by/4.0/). Source:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-4B4B-9
                            (CLARIN:EL)</ms:attributionText>
                    </ms:DatasetDistribution>
                    <ms:personalDataIncluded>false</ms:personalDataIncluded>
                    <ms:sensitiveDataIncluded>false</ms:sensitiveDataIncluded>
                </ms:LexicalConceptualResource>
            </ms:LRSubclass>
        </ms:LanguageResource>
    </ms:DescribedEntity>
	</ms:MetadataRecord>


.. _TrilingualLex:

Multilingual LCR
========================

.. raw:: html

    <button class="btn btn-example-data" onclick="$('#multiLexical').toggle(300)">Trilingual Term Dictionary</button><div id="multiLexical" style="display: none;">
	
.. code-block:: xml

	<?xml version="1.0" encoding="utf-8"?>
	<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://inventory.clarin.gr/metadata-schema/CLARIN-SHARE.xsd">
    <ms:metadataCreationDate>2019-03-27</ms:metadataCreationDate>
    <ms:metadataLastDateUpdated>2021-05-28</ms:metadataLastDateUpdated>
    <ms:metadataCurator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCurator>
    <ms:compliesWith>http://w3id.org/meta-share/meta-share/CLARIN-SHARE</ms:compliesWith>
    <ms:metadataCreator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCreator>
    <ms:sourceOfMetadataRecord>
        <ms:repositoryName xml:lang="el">Αποθετήριο ΕΚ ΑΘΗΝΑ</ms:repositoryName>
        <ms:repositoryName xml:lang="en">ATHENA RC Repository</ms:repositoryName>
        <ms:repositoryURL>http://inventory.clarin.gr</ms:repositoryURL>
    </ms:sourceOfMetadataRecord>
    <ms:DescribedEntity>
        <ms:LanguageResource>
            <ms:entityType>LanguageResource</ms:entityType>
            <ms:resourceName xml:lang="el">Τρίγλωσσο Ορολογικό Λεξικό (ΤΟΛ)</ms:resourceName>
            <ms:resourceName xml:lang="en">Trilingual Term Dictionary (TTD)</ms:resourceName>
            <ms:description xml:lang="el">Το Τρίγλωσσο Ορολογικό Λεξικό (ΤΟΛ) απευθύνεται σε
                αλλόφωνους μαθητές Γυμνασίου της μουσουλμανικής μειονότητας στη Θράκη με στόχο τη
                διευκόλυνση της μαθησιακής τους πορείας στο ελληνικό Γυμνάσιο. Περιλαμβάνει όρους
                που χρησιμοποιούνται στη διδασκαλία δεκατριών γνωστικών αντικειμένων –βιολογίας,
                γεωγραφίας, ιστορίας, κοινωνικής και πολιτικής αγωγής, λογοτεχνίας, μαθηματικών,
                μουσικής, νεοελληνικής γλώσσας, οικιακής οικονομίας, πληροφορικής, τεχνολογίας,
                φυσικής και χημείας– με βάση το αναλυτικό πρόγραμμα του Γυμνασίου. Οι όροι του ΤΟΛ
                έχουν συλλεχθεί από τα σχολικά εγχειρίδια που χρησιμοποιούνται στη διδασκαλία των
                αντίστοιχων μαθημάτων. Οι όροι είναι ταξινομημένοι ανά γνωστικό αντικείμενο και
                συνοδεύονται από εύληπτους και επιστημονικά ελεγμένους ορισμούς και μεταφράσεις στα
                Τουρκικά και τα Αγγλικά.</ms:description>
            <ms:description xml:lang="en">The Trilingual Term Dictionary (TTD) is targeted to
                foreign students of the secondary school in Thrace, Greece. The aim of the TTD is
                threefold: to assist the student in learning the subject areas of the curriculum, to
                improve their language skills in Greek and to familiarize themselves with
                information technology. TTD contains terms that are used in several subject areas
                (e.g. biology, geography, history, social and political studies etc.) that are
                taught in the secondary school. The terms of TDD (more than 5.000) have been
                collected from the schoolbooks. The terms are categorised within the subject areas,
                accompanied by definitions and translated into English and Turkish.</ms:description>
            <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                >http://hdl.handle.net/11500/ATHENA-0000-0000-5837-0</ms:LRIdentifier>
            <ms:version>1.0.0 (automatically assigned)</ms:version>
            <ms:additionalInfo>
                <ms:landingPage>http://www.ilsp.gr/tol/</ms:landingPage>
            </ms:additionalInfo>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:citationText xml:lang="el">Ινστιτούτο Επεξεργασίας του Λόγου - Ερευνητικό Κέντρο
                Αθηνά (2019). Τρίγλωσσο Ορολογικό Λεξικό (ΤΟΛ). Version 1.0.0 (automatically
                assigned). [Dataset (Lexical/Conceptual Resource)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-5837-0</ms:citationText>
            <ms:citationText xml:lang="en">Institute for Language and Speech Processing - Athena
                Research Center (2019). Trilingual Term Dictionary (TTD). Version 1.0.0
                (automatically assigned). [Dataset (Lexical/Conceptual Resource)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-5837-0</ms:citationText>
            <ms:keyword xml:lang="en">multilingual</ms:keyword>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">Mathematics</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC510</ms:DomainIdentifier>
            </ms:domain>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">Physics</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC530</ms:DomainIdentifier>
            </ms:domain>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">Biology</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC570</ms:DomainIdentifier>
            </ms:domain>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">Technology</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC600</ms:DomainIdentifier>
            </ms:domain>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">Home &amp; Family Management</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC640</ms:DomainIdentifier>
            </ms:domain>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">Chemistry</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC540</ms:DomainIdentifier>
            </ms:domain>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">Language</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC400</ms:DomainIdentifier>
            </ms:domain>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">Geography &amp; Travel</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC910</ms:DomainIdentifier>
            </ms:domain>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">Music</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC780</ms:DomainIdentifier>
            </ms:domain>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">History</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC900</ms:DomainIdentifier>
            </ms:domain>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">Literature, Rhetoric &amp;
                    Criticism</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC800</ms:DomainIdentifier>
            </ms:domain>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">Computer Science, Information &amp; General
                    Works</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/DDC_classification"
                    >DDC000</ms:DomainIdentifier>
            </ms:domain>
            <ms:resourceCreator>
                <ms:Organization>
                    <ms:actorType>Organization</ms:actorType>
                    <ms:organizationName xml:lang="el">Ινστιτούτο Επεξεργασίας του
                        Λόγου</ms:organizationName>
                    <ms:organizationName xml:lang="en">Institute for Language and Speech
                        Processing</ms:organizationName>
                    <ms:website>http://www.ilsp.gr/</ms:website>
                </ms:Organization>
            </ms:resourceCreator>
            <ms:fundingProject>
                <ms:projectName xml:lang="el">Τρίγλωσσο Ορολογικό Λεξικό</ms:projectName>
                <ms:projectName xml:lang="en">Trilingual Terminological Dictionary</ms:projectName>
                <ms:website>https://bit.ly/2V4hWLe</ms:website>
                <ms:website>https://www.ilsp.gr/projects/tol/</ms:website>
                <ms:fundingType>http://w3id.org/meta-share/meta-share/euFunds</ms:fundingType>
                <ms:fundingType>http://w3id.org/meta-share/meta-share/nationalFunds</ms:fundingType>
                <ms:funder>
                    <ms:Organization>
                        <ms:actorType>Organization</ms:actorType>
                        <ms:organizationName xml:lang="en">Ministry of Education and Religious
                            Affairs</ms:organizationName>
                    </ms:Organization>
                </ms:funder>
                <ms:funder>
                    <ms:Organization>
                        <ms:actorType>Organization</ms:actorType>
                        <ms:organizationName xml:lang="el">Ευρωπαϊκή Επιτροπή</ms:organizationName>
                        <ms:organizationName xml:lang="en">European Commission</ms:organizationName>
                        <ms:website>https://ec.europa.eu/info/index_en</ms:website>
                    </ms:Organization>
                </ms:funder>
            </ms:fundingProject>
            <ms:LRSubclass>
                <ms:LexicalConceptualResource>
                    <ms:lrType>LexicalConceptualResource</ms:lrType>
                    <ms:lcrSubclass>http://w3id.org/meta-share/meta-share/terminologicalResource</ms:lcrSubclass>
                    <ms:encodingLevel>http://w3id.org/meta-share/meta-share/other</ms:encodingLevel>
                    <ms:LexicalConceptualResourceMediaPart>
                        <ms:LexicalConceptualResourceTextPart>
                            <ms:lcrMediaType>LexicalConceptualResourceTextPart</ms:lcrMediaType>
                            <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
                            <ms:lingualityType>http://w3id.org/meta-share/meta-share/multilingual</ms:lingualityType>
                            <ms:language>
                                <ms:languageTag>el</ms:languageTag>
                                <ms:languageId>el</ms:languageId>
                            </ms:language>
                            <ms:language>
                                <ms:languageTag>en</ms:languageTag>
                                <ms:languageId>en</ms:languageId>
                            </ms:language>
                            <ms:language>
                                <ms:languageTag>tr</ms:languageTag>
                                <ms:languageId>tr</ms:languageId>
                            </ms:language>
                        </ms:LexicalConceptualResourceTextPart>
                    </ms:LexicalConceptualResourceMediaPart>
                    <ms:DatasetDistribution>
                        <ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
                        <ms:downloadLocation>http://www.hiddenLocation.org</ms:downloadLocation>
                        <ms:accessLocation>http://fixme.com</ms:accessLocation>
                        <ms:distributionTextFeature>
                            <ms:size>
                                <ms:amount>5224.0</ms:amount>
                                <ms:sizeUnit>http://w3id.org/meta-share/meta-share/entry</ms:sizeUnit>
                            </ms:size>
                            <ms:dataFormat>http://w3id.org/meta-share/omtd-share/Xml</ms:dataFormat>
                        </ms:distributionTextFeature>
                        <ms:licenceTerms>
                            <ms:licenceTermsName xml:lang="en">Creative Commons Attribution Non
                                Commercial No Derivatives 4.0 International</ms:licenceTermsName>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode</ms:licenceTermsURL>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by-nc-nd/4.0/</ms:licenceTermsURL>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/attribution</ms:conditionOfUse>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/nonCommercialUse</ms:conditionOfUse>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/noDerivatives</ms:conditionOfUse>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsDirectAccess</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/public</ms:licenceCategory>
                            <ms:LicenceIdentifier
                                ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX"
                                >CC-BY-NC-ND-4.0</ms:LicenceIdentifier>
                        </ms:licenceTerms>
                        <ms:attributionText xml:lang="el">Τρίγλωσσο Ορολογικό Λεξικό (ΤΟΛ).
                            Δημιουργός: Ινστιτούτο Επεξεργασίας του Λόγου - Ερευνητικό Κέντρο Αθηνά.
                            Άδεια: Creative Commons Attribution Non Commercial No Derivatives 4.0
                            International
                            (https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode,
                            https://creativecommons.org/licenses/by-nc-nd/4.0/). Πηγή:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-5837-0
                            (CLARIN:EL)</ms:attributionText>
                        <ms:attributionText xml:lang="en">Trilingual Term Dictionary (TTD) by
                            Institute for Language and Speech Processing - Athena Research Center
                            used under Creative Commons Attribution Non Commercial No Derivatives
                            4.0 International
                            (https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode,
                            https://creativecommons.org/licenses/by-nc-nd/4.0/). Source:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-5837-0
                            (CLARIN:EL)</ms:attributionText>
                    </ms:DatasetDistribution>
                    <ms:personalDataIncluded>false</ms:personalDataIncluded>
                    <ms:sensitiveDataIncluded>false</ms:sensitiveDataIncluded>
                </ms:LexicalConceptualResource>
            </ms:LRSubclass>
        </ms:LanguageResource>
    </ms:DescribedEntity>
	</ms:MetadataRecord>


.. _ToolXML:


3. Tool/Services
*******************

.. _LangIdentifier:

Single tool
================

.. raw:: html

    <button class="btn btn-example-data" onclick="$('#LangIdentifier').toggle(300)">ILSP Language Identification System</button>
    <div id="LangIdentifier" style="display: none;">

.. code-block:: xml


	<?xml version="1.0" encoding="utf-8"?>
	<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://inventory.clarin.gr/metadata-schema/CLARIN-SHARE.xsd">
    <ms:metadataCreationDate>2015-09-14</ms:metadataCreationDate>
    <ms:metadataLastDateUpdated>2021-05-28</ms:metadataLastDateUpdated>
    <ms:metadataCurator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCurator>
    <ms:compliesWith>http://w3id.org/meta-share/meta-share/CLARIN-SHARE</ms:compliesWith>
    <ms:metadataCreator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCreator>
    <ms:sourceOfMetadataRecord>
        <ms:repositoryName xml:lang="el">Αποθετήριο ΕΚ ΑΘΗΝΑ</ms:repositoryName>
        <ms:repositoryName xml:lang="en">ATHENA RC Repository</ms:repositoryName>
        <ms:repositoryURL>http://inventory.clarin.gr</ms:repositoryURL>
    </ms:sourceOfMetadataRecord>
    <ms:DescribedEntity>
        <ms:LanguageResource>
            <ms:entityType>LanguageResource</ms:entityType>
            <ms:resourceName xml:lang="el">Αναγνωριστής γλώσσας ψηφιακού κειμένου
                ΙΕΛ</ms:resourceName>
            <ms:resourceName xml:lang="en">ILSP Language Identification System</ms:resourceName>
            <ms:resourceShortName xml:lang="en">ILSP LangId</ms:resourceShortName>
            <ms:description xml:lang="el">Ο Αναγνωριστής γλώσσας του ΙΕΛ είναι ένα εργαλείο που
                χρησιμοποιείται για την αυτόματη αναγνώριση της γλώσσας ενός ψηφιακού κειμένου. Το
                εργαλείο αναγνωρίζει την ελληνική, αγγλική, γερμανική, γαλλική, ολλανδική γλώσσα
                καθώς και για τα greeklish, ενώ δίνεται και η δυνατότητα προσθήκης επιπλέον γλωσσών
                με την προσθήκη συμπληρωματικών αρχείων για κάθε γλώσσα.</ms:description>
            <ms:description xml:lang="en">The ILSP Language Identification System is a tool used for
                language identification in digital texts. The tool performs language identification
                for Greek, Greeklish, English, German, Dutch and French; it can also be used for
                other languages upon provision of specific supplementary external files for each new
                language.</ms:description>
            <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                >http://hdl.handle.net/11500/ATHENA-0000-0000-23E7-4</ms:LRIdentifier>
            <ms:version>1.0.0 (automatically assigned)</ms:version>
            <ms:additionalInfo>
                <ms:email>person@phs.uoa.gr</ms:email>
            </ms:additionalInfo>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:citationText xml:lang="el">Ινστιτούτο Επεξεργασίας του Λόγου - Ερευνητικό Κέντρο
                Αθηνά (2015). Αναγνωριστής γλώσσας ψηφιακού κειμένου ΙΕΛ. Version 1.0.0
                (automatically assigned). [Software (Tool/Service)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-23E7-4</ms:citationText>
            <ms:citationText xml:lang="en">Institute for Language and Speech Processing - Athena
                Research Center (2015). ILSP Language Identification System. Version 1.0.0
                (automatically assigned). [Software (Tool/Service)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-23E7-4</ms:citationText>
            <ms:keyword xml:lang="en">text</ms:keyword>
            <ms:resourceCreator>
                <ms:Organization>
                    <ms:actorType>Organization</ms:actorType>
                    <ms:organizationName xml:lang="el">Ινστιτούτο Επεξεργασίας του
                        Λόγου</ms:organizationName>
                    <ms:organizationName xml:lang="en">Institute for Language and Speech
                        Processing</ms:organizationName>
                    <ms:website>http://www.ilsp.gr/</ms:website>
                </ms:Organization>
            </ms:resourceCreator>
            <ms:intendedApplication>
                <ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LanguageTechnology</ms:LTClassRecommended>
            </ms:intendedApplication>
            <ms:intendedApplication>
                <ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LanguageIdentification</ms:LTClassRecommended>
            </ms:intendedApplication>
            <ms:isDocumentedBy>
                <ms:title xml:lang="el">Αναγνώριση γλώσσας ηλεκτρονικού κειμένου</ms:title>
                <ms:title xml:lang="en">Language identification in digital texts</ms:title>
                <ms:DocumentIdentifier
                    ms:DocumentIdentifierScheme="http://purl.org/spar/datacite/url"
                    >http://www.ilsp.gr/homepages/protopapas/pdf/Protopapas_2004_LangIDsubm.pdf</ms:DocumentIdentifier>
            </ms:isDocumentedBy>
            <ms:LRSubclass>
                <ms:ToolService>
                    <ms:lrType>ToolService</ms:lrType>
                    <ms:function>
                        <ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LanguageIdentification</ms:LTClassRecommended>
                    </ms:function>
                    <ms:SoftwareDistribution>
                        <ms:SoftwareDistributionForm>http://w3id.org/meta-share/meta-share/sourceCode</ms:SoftwareDistributionForm>
                        <ms:downloadLocation>http://www.hiddenLocation.org</ms:downloadLocation>
                        <ms:licenceTerms>
                            <ms:licenceTermsName xml:lang="en">BSD 2-Clause "Simplified"
                                License</ms:licenceTermsName>
                            <ms:licenceTermsURL>https://opensource.org/licenses/BSD-2-Clause</ms:licenceTermsURL>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/unspecified</ms:conditionOfUse>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsDirectAccess</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/public</ms:licenceCategory>
                            <ms:LicenceIdentifier
                                ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX"
                                >BSD-2-Clause</ms:LicenceIdentifier>
                        </ms:licenceTerms>
                        <ms:attributionText xml:lang="el">Αναγνωριστής γλώσσας ψηφιακού κειμένου
                            ΙΕΛ. Δημιουργός: Ινστιτούτο Επεξεργασίας του Λόγου - Ερευνητικό Κέντρο
                            Αθηνά. Άδεια: BSD 2-Clause "Simplified" License
                            (https://opensource.org/licenses/BSD-2-Clause). Πηγή:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-23E7-4
                            (CLARIN:EL)</ms:attributionText>
                        <ms:attributionText xml:lang="en">ILSP Language Identification System by
                            Institute for Language and Speech Processing - Athena Research Center
                            used under BSD 2-Clause "Simplified" License
                            (https://opensource.org/licenses/BSD-2-Clause). Source:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-23E7-4
                            (CLARIN:EL)</ms:attributionText>
                    </ms:SoftwareDistribution>
                    <ms:languageDependent>true</ms:languageDependent>
                    <ms:inputContentResource>
                        <ms:processingResourceType>http://w3id.org/meta-share/meta-share/corpus</ms:processingResourceType>
                        <ms:language>
                            <ms:languageTag>el-Latn</ms:languageTag>
                            <ms:languageId>el</ms:languageId>
                            <ms:scriptId>Latn</ms:scriptId>
                            <ms:languageVarietyName xml:lang="en">Greeklish</ms:languageVarietyName>
                        </ms:language>
                        <ms:language>
                            <ms:languageTag>el-Grek</ms:languageTag>
                            <ms:languageId>el</ms:languageId>
                            <ms:scriptId>Grek</ms:scriptId>
                        </ms:language>
                        <ms:language>
                            <ms:languageTag>fr</ms:languageTag>
                            <ms:languageId>fr</ms:languageId>
                        </ms:language>
                        <ms:language>
                            <ms:languageTag>en</ms:languageTag>
                            <ms:languageId>en</ms:languageId>
                        </ms:language>
                        <ms:language>
                            <ms:languageTag>de</ms:languageTag>
                            <ms:languageId>de</ms:languageId>
                        </ms:language>
                        <ms:language>
                            <ms:languageTag>nl</ms:languageTag>
                            <ms:languageId>nl</ms:languageId>
                        </ms:language>
                        <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
                    </ms:inputContentResource>
                    <ms:outputResource>
                        <ms:processingResourceType>http://w3id.org/meta-share/meta-share/corpus</ms:processingResourceType>
                        <ms:language>
                            <ms:languageTag>el-Latn</ms:languageTag>
                            <ms:languageId>el</ms:languageId>
                            <ms:scriptId>Latn</ms:scriptId>
                            <ms:languageVarietyName xml:lang="en">Greeklish</ms:languageVarietyName>
                        </ms:language>
                        <ms:language>
                            <ms:languageTag>el-Grek</ms:languageTag>
                            <ms:languageId>el</ms:languageId>
                            <ms:scriptId>Grek</ms:scriptId>
                        </ms:language>
                        <ms:language>
                            <ms:languageTag>fr</ms:languageTag>
                            <ms:languageId>fr</ms:languageId>
                        </ms:language>
                        <ms:language>
                            <ms:languageTag>en</ms:languageTag>
                            <ms:languageId>en</ms:languageId>
                        </ms:language>
                        <ms:language>
                            <ms:languageTag>de</ms:languageTag>
                            <ms:languageId>de</ms:languageId>
                        </ms:language>
                        <ms:language>
                            <ms:languageTag>nl</ms:languageTag>
                            <ms:languageId>nl</ms:languageId>
                        </ms:language>
                        <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
                    </ms:outputResource>
                    <ms:evaluated>false</ms:evaluated>
                </ms:ToolService>
            </ms:LRSubclass>
        </ms:LanguageResource>
    </ms:DescribedEntity>
	</ms:MetadataRecord>

.. _Voyant:

Combined tools
================

.. raw:: html

    <button class="btn btn-example-data" onclick="$('#ServiceInput').toggle(300)">Voyant Tools</button>
    <div id="ServiceInput" style="display: none;">

.. code-block:: xml

	<?xml version="1.0" encoding="utf-8"?>
	<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://inventory.clarin.gr/metadata-schema/CLARIN-SHARE.xsd">
	<ms:metadataCreationDate>2019-04-02</ms:metadataCreationDate>
	<ms:metadataLastDateUpdated>2021-05-28</ms:metadataLastDateUpdated>
	<ms:metadataCurator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCurator>
    <ms:compliesWith>http://w3id.org/meta-share/meta-share/CLARIN-SHARE</ms:compliesWith>
    <ms:metadataCreator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCreator>
    <ms:sourceOfMetadataRecord>
        <ms:repositoryName xml:lang="el">Αποθετήριο ΕΚ ΑΘΗΝΑ</ms:repositoryName>
        <ms:repositoryName xml:lang="en">ATHENA RC Repository</ms:repositoryName>
        <ms:repositoryURL>http://inventory.clarin.gr</ms:repositoryURL>
    </ms:sourceOfMetadataRecord>
    <ms:DescribedEntity>
        <ms:LanguageResource>
            <ms:entityType>LanguageResource</ms:entityType>
            <ms:resourceName xml:lang="en">Voyant Tools</ms:resourceName>
            <ms:description xml:lang="el">Τα Voyant Tools είναι ένα διαδικτυακό περιβάλλον ανοιχτού
                κώδικα που χρησιμοποιείται για την ανάγνωση και ανάλυση κειμένου ή/και σωμάτων
                κειμένων. Το περιβάλλον των Voyant Tools σχεδιάστηκε και υλοποιήθηκε από τους Stéfan
                Sinclair και Geoffrey Rockwell το 2016 με στόχο να διευκολύνει τους φοιτητές και
                ακαδημαϊκούς των ψηφιακών ανθρωπιστικών επιστημών αναφορικά με τις πρακτικές
                ανάγνωσης και ερμηνείας κειμενικού υλικού. Συγκεκριμένα, μπορείτε να χρησιμοποιήσετε
                τα Voyant Tools για: - να δείτε πώς επιτυγχάνεται η κειμενική ανάλυση με τη βοήθεια
                υπολογιστικών εργαλείων, - να μελετήσετε κείμενα που βρίσκετε στο διαδίκτυο ή
                κείμενα που έχετε επεξεργαστεί και τα οποία βρίσκονται αποθηκευμένα τοπικά στον Η/Υ
                σας, - να προσθέσετε επιπλέον λειτουργικότητες στις διαδικτυακές σας συλλογές,
                περιοδικά, ιστολόγια ή ιστότοπους, ώστε να μπορεί κανείς να δει το υλικό αυτό με τη
                βοήθεια εργαλείων ανάλυσης, - να προσθέσετε διαδραστικά στοιχεία στα κείμενα ή στα
                άρθρα που δημοσιεύετε στο διαδίκτυο ή ακόμα και να προσθέσετε διαδραστικά πάνελ στις
                ερευνητικές σας εκθέσεις (εφόσον αυτές μπορούν να δημοσιευθούν στο διαδίκτυο), ώστε
                οι αναγνώστες σας να μπορούν πολύ γρήγορα να βρουν τα βασικά σημεία και τα
                αποτελέσματα της έρευνάς σας, - να αναπτύξετε τα δικά σας εργαλεία χρησιμοποιώντας
                τις λειτουργικότητες και τον κώδικα των Voyant Tools.</ms:description>
            <ms:description xml:lang="en">Voyant Tools is a web-based text reading and analysis
                environment. It is a scholarly project that is designed to facilitate reading and
                interpretive practices for digital humanities students and scholars as well as for
                the general public. What you can do with Voyant: --Use it to learn how
                computers-assisted analysis works. Check out our examples that show you how to do
                real academic tasks with Voyant. --Use it to study texts that you find on the web or
                texts that you have carefully edited and have on your computer. --Use it to add
                functionality to your online collections, journals, blogs or web sites so others can
                see through your texts with analytical tools. --Use it to add interactive evidence
                to your essays that you publish online. Add interactive panels right into your
                research essays (if they can be published online) so your readers can recapitulate
                your results. --Use it to develop your own tools using our functionality and
                code.</ms:description>
            <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                >http://hdl.handle.net/11500/ATHENA-0000-0000-5827-2</ms:LRIdentifier>
            <ms:version>1.0.0 (automatically assigned)</ms:version>
            <ms:additionalInfo>
                <ms:landingPage>https://voyant-tools.org/</ms:landingPage>
            </ms:additionalInfo>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:citationText xml:lang="en">Rockwell, Geoffrey; Sinclair, Stéfan (2019). Voyant
                Tools. Version 1.0.0 (automatically assigned). [Software (Tool/Service)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-5827-2</ms:citationText>
            <ms:keyword xml:lang="en">text</ms:keyword>
            <ms:resourceCreator>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:resourceCreator>
            <ms:resourceCreator>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:resourceCreator>
            <ms:LRSubclass>
                <ms:ToolService>
                    <ms:lrType>ToolService</ms:lrType>
                    <ms:function>
                        <ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LinguisticAnalysis</ms:LTClassRecommended>
                    </ms:function>
                    <ms:SoftwareDistribution>
                        <ms:SoftwareDistributionForm>http://w3id.org/meta-share/meta-share/webService</ms:SoftwareDistributionForm>
                        <ms:executionLocation>https://voyant-tools.org/</ms:executionLocation>
                        <ms:webServiceType>http://w3id.org/meta-share/meta-share/unspecified</ms:webServiceType>
                        <ms:licenceTerms>
                            <ms:licenceTermsName xml:lang="en">GNU General Public License v3.0 or
                                later</ms:licenceTermsName>
                            <ms:licenceTermsURL>https://www.gnu.org/licenses/gpl-3.0-standalone.html</ms:licenceTermsURL>
                            <ms:licenceTermsURL>https://opensource.org/licenses/GPL-3.0</ms:licenceTermsURL>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/unspecified</ms:conditionOfUse>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsDirectAccess</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/public</ms:licenceCategory>
                            <ms:LicenceIdentifier
                                ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX"
                                >GPL-3.0-or-later</ms:LicenceIdentifier>
                        </ms:licenceTerms>
                        <ms:licenceTerms>
                            <ms:licenceTermsName xml:lang="en">Creative Commons Attribution 4.0
                                International</ms:licenceTermsName>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by/4.0/legalcode</ms:licenceTermsURL>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by/4.0/</ms:licenceTermsURL>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/attribution</ms:conditionOfUse>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsDirectAccess</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsProcessing</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/public</ms:licenceCategory>
                            <ms:LicenceIdentifier
                                ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX"
                                >CC-BY-4.0</ms:LicenceIdentifier>
                        </ms:licenceTerms>
                        <ms:attributionText xml:lang="el">Voyant Tools. Δημιουργός: Geoffrey
                            Rockwell and Stéfan Sinclair. Άδεια: GNU General Public License v3.0 or
                            later (https://www.gnu.org/licenses/gpl-3.0-standalone.html,
                            https://opensource.org/licenses/GPL-3.0) and Creative Commons
                            Attribution 4.0 International
                            (https://creativecommons.org/licenses/by/4.0/legalcode,
                            https://creativecommons.org/licenses/by/4.0/). Πηγή:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-5827-2
                            (CLARIN:EL)</ms:attributionText>
                        <ms:attributionText xml:lang="en">Voyant Tools by Geoffrey Rockwell and
                            Stéfan Sinclair used under GNU General Public License v3.0 or later
                            (https://www.gnu.org/licenses/gpl-3.0-standalone.html,
                            https://opensource.org/licenses/GPL-3.0) and Creative Commons
                            Attribution 4.0 International
                            (https://creativecommons.org/licenses/by/4.0/legalcode,
                            https://creativecommons.org/licenses/by/4.0/). Source:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-5827-2
                            (CLARIN:EL)</ms:attributionText>
                    </ms:SoftwareDistribution>
                    <ms:languageDependent>false</ms:languageDependent>
                    <ms:inputContentResource>
                        <ms:processingResourceType>http://w3id.org/meta-share/meta-share/corpus</ms:processingResourceType>
                        <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
                        <ms:dataFormat>http://w3id.org/meta-share/omtd-share/Pdf</ms:dataFormat>
                        <ms:dataFormat>http://w3id.org/meta-share/omtd-share/Rtf</ms:dataFormat>
                        <ms:dataFormat>http://w3id.org/meta-share/omtd-share/Xml</ms:dataFormat>
                        <ms:dataFormat>http://w3id.org/meta-share/omtd-share/ConllU</ms:dataFormat>
                        <ms:dataFormat>http://w3id.org/meta-share/omtd-share/Html</ms:dataFormat>
                    </ms:inputContentResource>
                    <ms:outputResource>
                        <ms:processingResourceType>http://w3id.org/meta-share/meta-share/corpus</ms:processingResourceType>
                        <ms:mediaType>http://w3id.org/meta-share/meta-share/image</ms:mediaType>
                    </ms:outputResource>
                    <ms:outputResource>
                        <ms:processingResourceType>http://w3id.org/meta-share/meta-share/corpus</ms:processingResourceType>
                        <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
                    </ms:outputResource>
                    <ms:evaluated>false</ms:evaluated>
                </ms:ToolService>
    </ms:LRSubclass>
    </ms:LanguageResource>
	</ms:DescribedEntity>
	</ms:MetadataRecord>

.. _LDXML:

4. Language Descriptions
**************************

.. raw:: html

    <button class="btn btn-example-data" onclick="$('#Grammar').toggle(300)">PANACEA Environment Corpus n-grams EL</button>
    <div id="Grammar" style="display: none;">

.. code-block:: xml

	<?xml version="1.0" encoding="utf-8"?>
	<ms:MetadataRecord xmlns:ms="http://w3id.org/meta-share/meta-share/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ https://inventory.clarin.gr/metadata-schema/CLARIN-SHARE.xsd">
    <ms:metadataCreationDate>2015-09-10</ms:metadataCreationDate>
    <ms:metadataLastDateUpdated>2021-05-28</ms:metadataLastDateUpdated>
    <ms:metadataCurator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCurator>
    <ms:compliesWith>http://w3id.org/meta-share/meta-share/CLARIN-SHARE</ms:compliesWith>
    <ms:metadataCreator>
        <ms:actorType>Person</ms:actorType>
        <ms:surname xml:lang="en">Person_Surname</ms:surname>
        <ms:givenName xml:lang="en">Person_Name</ms:givenName>
    </ms:metadataCreator>
    <ms:sourceOfMetadataRecord>
        <ms:repositoryName xml:lang="el">Αποθετήριο ΕΚ ΑΘΗΝΑ</ms:repositoryName>
        <ms:repositoryName xml:lang="en">ATHENA RC Repository</ms:repositoryName>
        <ms:repositoryURL>http://inventory.clarin.gr</ms:repositoryURL>
    </ms:sourceOfMetadataRecord>
    <ms:DescribedEntity>
        <ms:LanguageResource>
            <ms:entityType>LanguageResource</ms:entityType>
            <ms:resourceName xml:lang="el">PANACEA σώμα ελληνικών n-γραμμάτων (n-grams)
                περιβαλλοντικού τομέα</ms:resourceName>
            <ms:resourceName xml:lang="en">PANACEA Environment Corpus n-grams EL
                (Greek)</ms:resourceName>
            <ms:description xml:lang="el">Ο συγκεκριμένος πόρος περιλαμβάνει ελληνικά n-γράμματα
                (για n = 1 – 5) λέξεων και n-γράμματα λέξης/μορφοσυντακτικού χαρακτηρισμού/λήμματος
                στον τομέα του περιβάλλοντος, τα οποία συνοδεύονται από τις αντίστοιχες συχνότητες
                εμφάνισης στο σώμα κειμένων. Ο πόρος αναπτύχθηκε στο πλαίσιο του έργου PANACEA
                (http://www.panacea-lr.eu), που χρηματοδοτήθηκε από το 7ο ΠΠ. Βασίστηκε σε
                ιστοσελίδες που ανακτήθηκαν αυτόματα από το διαδίκτυο με χρήση του εργαλείου FPC,
                αφού αναγνωρίστηκε αυτόματα ότι τα κείμενα είναι γραμμένα στην ελληνική και είναι
                σχετικά με το περιβάλλον. Το κειμενικό σώμα αποτελείται από περίπου 31,71 εκατ.
                μονάδες. Η συλλογή των δεδομένων έγινε το καλοκαίρι του 2011.</ms:description>
            <ms:description xml:lang="en">PANACEA Environment Corpus n-grams EL (Greek) 1.0 contains
                Greek word n-grams and Greek word/tag/lemma n-grams in the "Environment" (ENV)
                domain. N-grams are accompanied by their observed frequency counts. The length of
                the n-grams ranges from unigrams (single words) to five-grams. The data were
                collected in the context of PANACEA (http://www.panacea-lr.eu), an EU-FP7 Funded
                Project under Grant Agreement 248064. The n-gram counts were generated from crawled
                Web pages that were automatically detected to be in the Greek language and were
                automatically classified as relevant to the ENV domain. The collection consisted of
                approximately 31.71 million tokens. Data collection took place in the summer of
                2011.</ms:description>
            <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/handle"
                >http://hdl.handle.net/11500/ATHENA-0000-0000-23DA-3</ms:LRIdentifier>
            <ms:version>1.0</ms:version>
            <ms:additionalInfo>
                <ms:landingPage>http://nlp.ilsp.gr/panacea/D4.3/data/201209/gms/env_el/README.txt</ms:landingPage>
            </ms:additionalInfo>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:contact>
                <ms:Person>
                    <ms:actorType>Person</ms:actorType>
                    <ms:surname xml:lang="en">Person_Surname</ms:surname>
                    <ms:givenName xml:lang="en">Person_Name</ms:givenName>
                </ms:Person>
            </ms:contact>
            <ms:citationText xml:lang="el">Ινστιτούτο Επεξεργασίας του Λόγου - Ερευνητικό Κέντρο
                Αθηνά (2015). PANACEA σώμα ελληνικών n-γραμμάτων (n-grams) περιβαλλοντικού τομέα.
                Version 1.0. [Model (n-gram model)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-23DA-3</ms:citationText>
            <ms:citationText xml:lang="en">Institute for Language and Speech Processing - Athena
                Research Center (2015). PANACEA Environment Corpus n-grams EL (Greek). Version 1.0.
                [Model (n-gram model)]. CLARIN:EL.
                http://hdl.handle.net/11500/ATHENA-0000-0000-23DA-3</ms:citationText>
            <ms:keyword xml:lang="en">monolingual</ms:keyword>
            <ms:domain>
                <ms:categoryLabel xml:lang="en">environment</ms:categoryLabel>
                <ms:DomainIdentifier
                    ms:DomainClassificationScheme="http://w3id.org/meta-share/meta-share/ClarinEL_domainClassification"
                    >Clarin_Domain002</ms:DomainIdentifier>
            </ms:domain>
            <ms:resourceCreator>
                <ms:Organization>
                    <ms:actorType>Organization</ms:actorType>
                    <ms:organizationName xml:lang="el">Ινστιτούτο Επεξεργασίας του
                        Λόγου</ms:organizationName>
                    <ms:organizationName xml:lang="en">Institute for Language and Speech
                        Processing</ms:organizationName>
                    <ms:website>http://www.ilsp.gr/</ms:website>
                </ms:Organization>
            </ms:resourceCreator>
            <ms:creationStartDate>2011-06-01</ms:creationStartDate>
            <ms:creationEndDate>2011-08-31</ms:creationEndDate>
            <ms:fundingProject>
                <ms:projectName xml:lang="en">Platform for Automatic, Normalized Annotation and
                    Cost-Effective Acquisition of Language Resources for Human
                    Language</ms:projectName>
                <ms:website>http://www.panacea-lr.eu</ms:website>
                <ms:website>http://panacea-lr.eu/</ms:website>
                <ms:grantNumber>ICT-248064</ms:grantNumber>
                <ms:fundingType>http://w3id.org/meta-share/meta-share/euFunds</ms:fundingType>
                <ms:funder>
                    <ms:Organization>
                        <ms:actorType>Organization</ms:actorType>
                        <ms:organizationName xml:lang="el">Ευρωπαϊκή Επιτροπή</ms:organizationName>
                        <ms:organizationName xml:lang="en">European Commission</ms:organizationName>
                        <ms:website>https://ec.europa.eu/info/index_en</ms:website>
                    </ms:Organization>
                </ms:funder>
            </ms:fundingProject>
            <ms:creationDetails xml:lang="en">automatic web crawling, automatic language detection,
                data preprocessing (boilerpipe filtering, lemmatization &amp;
                tagging)</ms:creationDetails>
            <ms:isCreatedBy>
                <ms:resourceName xml:lang="en">boilerpipe library</ms:resourceName>
                <ms:LRIdentifier ms:LRIdentifierScheme="http://purl.org/spar/datacite/url"
                    >https://code.google.com/archive/p/boilerpipe/</ms:LRIdentifier>
                <ms:version>unspecified</ms:version>
            </ms:isCreatedBy>
            <ms:isCreatedBy>
                <ms:resourceName xml:lang="en">ILSP Lemmatizer</ms:resourceName>
                <ms:version>unspecified</ms:version>
            </ms:isCreatedBy>
            <ms:isCreatedBy>
                <ms:resourceName xml:lang="en">ILSP Feature-based multi-tiered POS
                    Tagger</ms:resourceName>
                <ms:version>unspecified</ms:version>
            </ms:isCreatedBy>
            <ms:isDocumentedBy>
                <ms:title xml:lang="en">PANACEA Environment Corpus n-grams EL 1.0 README</ms:title>
                <ms:DocumentIdentifier
                    ms:DocumentIdentifierScheme="http://purl.org/spar/datacite/url"
                    >http://nlp.ilsp.gr/panacea/D4.3/data/201209/gms/env_el/README.txt</ms:DocumentIdentifier>
            </ms:isDocumentedBy>
            <ms:LRSubclass>
                <ms:LanguageDescription>
                    <ms:lrType>LanguageDescription</ms:lrType>
                    <ms:LanguageDescriptionSubclass>
                        <ms:NGramModel>
                            <ms:ldSubclassType>NGramModel</ms:ldSubclassType>
                            <ms:baseItem>http://w3id.org/meta-share/meta-share/word</ms:baseItem>
                            <ms:order>5</ms:order>
                        </ms:NGramModel>
                    </ms:LanguageDescriptionSubclass>
                    <ms:LanguageDescriptionMediaPart>
                        <ms:LanguageDescriptionTextPart>
                            <ms:ldMediaType>LanguageDescriptionTextPart</ms:ldMediaType>
                            <ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
                            <ms:lingualityType>http://w3id.org/meta-share/meta-share/monolingual</ms:lingualityType>
                            <ms:language>
                                <ms:languageTag>el</ms:languageTag>
                                <ms:languageId>el</ms:languageId>
                            </ms:language>
                        </ms:LanguageDescriptionTextPart>
                    </ms:LanguageDescriptionMediaPart>
                    <ms:DatasetDistribution>
                        <ms:DatasetDistributionForm>http://w3id.org/meta-share/meta-share/downloadable</ms:DatasetDistributionForm>
                        <ms:downloadLocation>http://www.hiddenLocation.org</ms:downloadLocation>
                        <ms:samplesLocation>http://nlp.ilsp.gr/panacea/D4.3/data/201209/gms/env_el/ENV_EL_1000.3gms.sample</ms:samplesLocation>
                        <ms:samplesLocation>http://nlp.ilsp.gr/panacea/D4.3/data/201209/gms/env_el/ENV_EL_wpl_1000.3gms.sample</ms:samplesLocation>
                        <ms:distributionTextFeature>
                            <ms:size>
                                <ms:amount>14954020.0</ms:amount>
                                <ms:sizeUnit>http://w3id.org/meta-share/meta-share/five-gram</ms:sizeUnit>
                            </ms:size>
                            <ms:size>
                                <ms:amount>13683940.0</ms:amount>
                                <ms:sizeUnit>http://w3id.org/meta-share/meta-share/four-gram</ms:sizeUnit>
                            </ms:size>
                            <ms:size>
                                <ms:amount>3860716.0</ms:amount>
                                <ms:sizeUnit>http://w3id.org/meta-share/meta-share/bigram</ms:sizeUnit>
                            </ms:size>
                            <ms:size>
                                <ms:amount>9767383.0</ms:amount>
                                <ms:sizeUnit>http://w3id.org/meta-share/meta-share/trigram</ms:sizeUnit>
                            </ms:size>
                            <ms:size>
                                <ms:amount>435189.0</ms:amount>
                                <ms:sizeUnit>http://w3id.org/meta-share/meta-share/unigram</ms:sizeUnit>
                            </ms:size>
                            <ms:dataFormat>http://w3id.org/meta-share/omtd-share/Text</ms:dataFormat>
                        </ms:distributionTextFeature>
                        <ms:licenceTerms>
                            <ms:licenceTermsName xml:lang="en">Creative Commons Attribution Share
                                Alike 4.0 International</ms:licenceTermsName>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by-sa/4.0/legalcode</ms:licenceTermsURL>
                            <ms:licenceTermsURL>https://creativecommons.org/licenses/by-sa/4.0/</ms:licenceTermsURL>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/attribution</ms:conditionOfUse>
                            <ms:conditionOfUse>http://w3id.org/meta-share/meta-share/shareAlike</ms:conditionOfUse>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsDirectAccess</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/allowsProcessing</ms:licenceCategory>
                            <ms:licenceCategory>http://w3id.org/meta-share/meta-share/public</ms:licenceCategory>
                            <ms:LicenceIdentifier
                                ms:LicenceIdentifierScheme="http://w3id.org/meta-share/meta-share/SPDX"
                                >CC-BY-SA-4.0</ms:LicenceIdentifier>
                        </ms:licenceTerms>
                        <ms:attributionText xml:lang="el">PANACEA σώμα ελληνικών n-γραμμάτων
                            (n-grams) περιβαλλοντικού τομέα. Δημιουργός: Ινστιτούτο Επεξεργασίας του
                            Λόγου - Ερευνητικό Κέντρο Αθηνά. Άδεια: Creative Commons Attribution
                            Share Alike 4.0 International
                            (https://creativecommons.org/licenses/by-sa/4.0/legalcode,
                            https://creativecommons.org/licenses/by-sa/4.0/). Πηγή:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-23DA-3
                            (CLARIN:EL)</ms:attributionText>
                        <ms:attributionText xml:lang="en">PANACEA Environment Corpus n-grams EL
                            (Greek) by Institute for Language and Speech Processing - Athena
                            Research Center used under Creative Commons Attribution Share Alike 4.0
                            International (https://creativecommons.org/licenses/by-sa/4.0/legalcode,
                            https://creativecommons.org/licenses/by-sa/4.0/). Source:
                            http://hdl.handle.net/11500/ATHENA-0000-0000-23DA-3
                            (CLARIN:EL)</ms:attributionText>
                    </ms:DatasetDistribution>
                    <ms:personalDataIncluded>false</ms:personalDataIncluded>
                    <ms:sensitiveDataIncluded>false</ms:sensitiveDataIncluded>
                </ms:LanguageDescription>
            </ms:LRSubclass>
        </ms:LanguageResource>
    </ms:DescribedEntity>
	</ms:MetadataRecord>

