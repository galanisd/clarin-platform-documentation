.. _GuidelinesGen:

##################################
General guidelines on metadata
##################################

.. raw:: html

	<p style="border:2px; border-style:solid; border-color:#c4bfb3; border-radius: 10px; box-shadow: 5px 5px 3px 1px #999; padding: 0.5em; font-family:calibri light; font-size:17px;"><i><b>In this chapter:</b> metadata completeness; consistency with data; information in languages other than English; best practices on editing/versioning a metadata record; what to avoid.</i><br></p>


Language
****************

All metadata elements are presented and must be filled out, primarily, in *English*. Nonetheless, you can  provide information in **any other language**. To do so, click on the **add symbol [+]** next to the metadata you wish to describe in a different language (e.g. LRT name).

.. image:: AddLanguage1.png
	:width: 1200px

The metadata field is duplicated and the new language in which you can provide information is, by default, *Modern Greek*. 

.. image:: AddLanguage2.png
	:width: 1200px

.. attention:: If this is not the case, **select another language** from the drop down list. 

.. image:: AddLanguage3.png
	:width: 1200px

If you decide to **remove the added field**, click on the **remove symbol [x]**. This action will remove the whole field, i.e. LRT name, not just the language.

Consistency
***************

The metadata used to describe your data should **clearly reflect them**. Make sure there are no inconsistencies (e.g. check that your files are indeed in PDF format and not just scanned images; if you provide information on an annotated corpus, indicate the annotation tool etc.). Check first which are the :ref:`mandatory <Mandatory>` metadata per resource type and then see :ref:`how to fill them in <GuidelinesSpec>`.

Completeness
*****************

Make sure you provide all the necessary information in all selected languages. It is highly recommended that you provide the title and the description in **English** and **Greek** (along with any other language, if needed) as the search and the resource retrieval is facilitated. 

Editing
************

Editing metadata is necessary when there are typos, inconsistencies, missing information etc. If you are a :ref:`curator <Curator>`, you can :ref:`edit <Edit>` the resource metadata as many times as you want before submitting the record for publication. Once submitted, editing is permitted only to the :ref:`supervisor <Supervisor>` (see :ref:`here <ActFrom>` the differences in actions per role type). 

.. attention:: Keep in mind that **editing** concerns only **minor changes** in the metadata of the resource in question. Editing takes place during the creation of a resource until it is submitted for publication. It differs from versioning in that the **changes do not constitute a new entity**. For example, if there has been a *mistake in the resource size*, **editing** is required while *if the resource size has changed* (as is the case with glossaries which are augmented) a **new version** is needed. 

.. _Versioning:

Versioning
*************

If there are signifant changes to the resource which differentiate it from the existing entity described, then a new version must be created. Such changes are *new editions of glossaries with more terms, new improved cleaned versions of corpora, new data (e.g. parallel sentences) with which a bilingual corpus has been enriched or tool/software updates*. Both the :ref:`curator <Curator>` and the :ref:`supervisor <Supervisor>` can :ref:`create a new version <CreateNew>` after the resource has been published. 

.. attention:: Keep in mind that only the **latest version** is found in the central inventory, while *all versions* are accesible from the **resource view page**, as shown in the image below.

.. image:: Versions.png
    :width: 1200px
